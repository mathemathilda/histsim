#include "helper.hpp"


/*
std::vector<std::string> explode(const std::string& str, const std::string& delimiters = " ")
{
    std::vector<std::string> tokens;
 
    int subStrBeginPos = str.find_first_not_of(delimiters, 0);
    int subStrEndPos = str.find_first_of(delimiters, subStrBeginPos);
 
    while(std::string::npos != subStrBeginPos || std::string::npos != subStrEndPos)
    {
        tokens.push_back(str.substr(subStrBeginPos, subStrEndPos-subStrBeginPos));
 
        subStrBeginPos = str.find_first_not_of(delimiters, subStrEndPos);
        subStrEndPos = str.find_first_of(delimiters, subStrBeginPos);
    }
 
    return tokens;
}


*/

void printVector(std::vector<std::string> vector)
{
	int size = vector.size();
	for(int i = 0; i < size; i++)
	{
		std::cout << "Element: " << i << " Value: " << vector[i] << "\n";
	}
}




void printVector(std::vector<int> vector)
{
	int size = vector.size();
	for(int i = 0; i < size; i++)
	{
		//std::cout << "Element: " << i << " Value: " << vector[i] << "\n";
		std::cout << vector[i] << "\t";
	}
	std::cout << "\n";
}



void printVector(std::vector<float> vector)
{
	int size = vector.size();
	for(int i = 0; i < size; i++)
	{
		//std::cout << "Element: " << i << " Value: " << vector[i] << "\n";
		std::cout << vector[i] << "\t";
	}
	std::cout << "\n";
}
