#ifndef HELPER_HPP_
#define HELPER_HPP_


#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <set>

#include <boost/variant/variant.hpp>
#include <boost/lexical_cast.hpp>


//#include "chromatin.hpp"

//std::vector<std::string> explode(const std::string& str, const std::string& delimiters );


inline void printMatrixToFile(std::vector<std::vector<float> > &matrix, std::ofstream& Handle);


void printVector(std::vector<std::string>);
void printVector(std::vector<int>);
void printVector(std::vector<float>);



//void printChromatinState(chromatin cChromatin, std::string sFile = "NULL");


#endif /* HELPER_HPP_ */
