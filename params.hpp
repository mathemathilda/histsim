#ifndef PARAMS_HPP_
#define PARAMS_HPP_
#include <vector>
#include <string>

namespace params
{
    extern std::vector<std::string> modvector;
	extern std::vector<std::string> sitevector;
    extern std::vector<std::vector< std::string>> modmatrix;
};

#endif // PARAMS_HPP_
