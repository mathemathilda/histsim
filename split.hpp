/*
 * split.hpp
 *
 *  Created on: Jul 12, 2016
 *      Author: nicoh
 */

#ifndef SPLIT_HPP_
#define SPLIT_HPP_

#include <vector>
#include <string>
#include "histone.hpp"
std::vector<std::string> explode(std::string input, std::string delimiter);
std::vector<std::string> decomposeTarget(std::string input);


bool mNucMatch(std::vector<Hist> &vNucleosomeHist, std::vector<Hist> &vMatchingHist);
std::vector<std::vector<std::string> > parseStateOccurences(std::string &sInput);

void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace);


#endif /* SPLIT_HPP_ */
