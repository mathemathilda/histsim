/*
 * nucleosome.hpp
 *
 *  Created on: Apr 23, 2015
 *      Author: nicoh
 */

#ifndef NUCLEOSOME_HPP_
#define NUCLEOSOME_HPP_

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>
#include "helper.hpp"
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include "split.hpp"
#include "histone.hpp"
#include "simparams.hpp"

class cNucleosome
{
	public:
        std::string printString(void);
       // std::string printIntString(void);

        uint iPosition;
		std::vector<std::string> vHistoneNames;
        //std::vector<std::string> sNucleosomeString;
        std::vector<Hist> nucleosomeHist;

};


#endif /* NUCLEOSOME_HPP_ */
