#ifndef HISTSIM_HPP_
#define HISTSIM_HPP_

#include <sys/time.h>
#include <ctime>
//#include <cilk/cilk.h>
#include "histone.hpp"

typedef long long int64;
typedef unsigned long long uint64;
typedef unsigned int uint;

inline uint64 GetTimeMs64()		//ordentlich loesen
{
	struct timeval tv;

	gettimeofday(&tv, 0);

	uint64 ret = tv.tv_usec;
 /* Convert from micro seconds (10^-6) to milliseconds (10^-3) */
	ret /= 1000;

 /* Adds the seconds (10^0) after converting them to milliseconds (10^-3) */
	ret += (tv.tv_sec * 1000);

	return ret;
}

#endif /* HISTSIM_HPP_ */
