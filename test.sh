#! /bin/bash

make clean 
rm HistSim
if [[ ! $(make) ]]; then 
	echo "failed to build"
	exit 0
fi	
rm   simout  nondyn_outfile.txt -r
./HistSim --rf ../histsim/original_simout/00ff7e1590794e4cbfd4b574a817828f.cfg --sf ../histsim/original_simout/statefile --pf ../histsim/original_simout/paramfile --nondyn --seed 42
mv simout/0/outfile.txt nondyn_outfile.txt

rm   simout -r
./HistSim --rf original_simout/00ff7e1590794e4cbfd4b574a817828f.cfg --sf original_simout/statefile --pf original_simout/paramfile --seed 42

fileOne=original_simout/original_nondynamic_outfile
fileTwo=nondyn_outfile.txt

# last line only
if [[ $(diff -w <(tail -n 1 $fileOne ) <(tail -n 1 $fileTwo)) ]]; then 

# whole document
#if [[ $(diff -w $fileOne $fileTwo) ]]; then 
	echo "########### nondyn different #############"
	nondyn=1
else 
	echo "       nondyn same"
	nondyn=0
fi

# last line only
if [[ $(diff -w <(tail -n 1 original_simout/outfile.txt 	) <(tail -n 1 simout/0/outfile.txt)) ]]; then 

# whole document
#if [[ $(diff -w $fileOne $fileTwo) ]]; then 
	echo "########### dynamic concentrations different #############"
	dyn=1
else 
	echo "       dynamic concentrations same"
	dyn=0
fi

# exit 0 if both tests passed
exit  $(($nondyn + $dyn ))
