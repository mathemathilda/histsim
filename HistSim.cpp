//============================================================================
// Name        : HistSim.cpp
// Author      : Nico Herbig and others :)
// Version     : 0.1
// Copyright   : (C) 2015 Nico Herbig
// Description : Main file for histone simulation
//============================================================================

#include <iostream>
#include <fstream>
//#include <map>
//#include <string>
//#include <boost/variant/variant.hpp>
//#include <boost/variant/get.hpp>
//#include <boost/lexical_cast.hpp>
#include "cXMLParser.hpp"
//#include "helper.hpp"
#include "chromatin.hpp"
#include "rule.hpp"
//#include "cEnzyme.hpp"
#include "cEnzymeSet.hpp"
#include "gillespie.hpp"
//#include <cstring>
//#include <boost/filesystem.hpp>
#include <dirent.h>
#include <ctime>
#include "HistSim.hpp"
#include "split.hpp"
#include <boost/algorithm/string.hpp>
#include "CLI11.hpp"
#include <random>
#include <cstdio>
//#include "simparams.hpp"

//OS specific includes
#ifdef Q_OS_WIN32
#include <windows.h>
#endif

//#ifdef Q_OS_LINUX
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
//#endif

//parallel computing
//#include <omp.h>

// copy in binary mode
bool copyFile(const std::string SRC, const std::string DEST)
{
    std::ifstream src(SRC, std::ios::binary);
    std::ofstream dest(DEST, std::ios::binary);
    dest << src.rdbuf();
    return src && dest;
}


int main(int argc, char* argv[])
{
    CLI::App histsim{"App description"};
    Simparams sp;
    Simvars sv;
    bool bCheck;
    sp.outputmode = 1;
    sv.outputIteration= true;
    sp.writeSteps = 0;
    int iNoOverride = 0;
    std::string stateFile;
    std::string ruleFile;
    std::string paramFile;
    std::string outputDir = "simout/";
    std::string histoneMapFile;
    sp.nRuns =1;
    /****************************
    /* Define commandline options
    /****************************/

    CLI::Option* verbosityOpt  =  histsim.add_option("-v", sp.verbosity, "verbosity level: \n\t\t\t\t "
                                           "1:log general simulation progress information \n\t\t\t\t "
                                           "2:log general algorithm actions \n\t\t\t\t "
                                           "3: log everything the algorithm does (ultra verbose) ");
    //CLI::Option* expandOpt = histsim.add_option("-e,--expand", expandfolder, " rerun simulation with standard output mode");
    CLI::Option* stateFileOpt = histsim.add_option("--sf", stateFile, "State file (REQUIRED FOR SIMULATION AND COMPLIANCE CHECK!)");
    CLI::Option* ruleFileOpt = histsim.add_option("--rf", ruleFile, "Rule file (REQUIRED FOR SIMULATION AND COMPLIANCE CHECK!)");
    CLI::Option* paramFileOpt =histsim.add_option("--pf", paramFile, "Parameter file (REQUIRED FOR SIMULATION AND COMPLIANCE CHECK!)");
    histsim.add_option("-o,--outputmode",sp.outputmode, "output file format \n\t\t\t\t "
                                                        "0: minimal output (faster) \n\t\t\t\t "
                                                        "1: standard output (default) \n\t\t\t\t "
                                                        "2: progressive output \n\t\t\t\t "
                                                        "3: both standard and progressive output formats");
    CLI::Option* matrixlogOpt  = histsim.add_flag("--pm", sp.matrixlog, "output propensity matrices");
    CLI::Option* outputDirOpt =histsim.add_option("-d",outputDir, "output directory (standard: simout)",1);
    CLI::Option* histoneMapFileOpt = histsim.add_option("--hm",histoneMapFile, "histone map (REQUIRED FOR COMPLIANCE CHECK!)");
    histsim.add_flag("--check", bCheck, "perform compliance check, --hm and --sf or --rf required")
            ->needs(histoneMapFileOpt);
    CLI::Option* SetSimTimeOpt = histsim.add_option("-t",sp.SetSimTime, "stop after given simulation timesteps");
    CLI::Option* SetIterationCountOpt = histsim.add_option("-i",sp.SetIterationCount, "stop after given number of iterations, warning! this is not implemented yet");
    histsim.add_option("--write", sp.writeTimeSteps, "list of time steps to write output in long output format,\n\t\t\t\t"
                                                                                 "reduces steps printed with normal output, \n\t\t\t\t "
                                                                                 "adds additional output lines to minial output ");
    histsim.add_option("--steps", sp.writeSteps, "write every nth timestep to minimal output format ");
    histsim.add_option("-s",sp.initstate, "starting initial state, warning! this is not implemented yet");
    histsim.add_option("--tdr", sp.tdr, "set timedomain resolution ");
    histsim.add_option("-r,--runs", sp.nRuns, "number of runs to perform",1);
    histsim.add_flag("--nondyn", sp.nonDynamiConcentrations, " do not recalculate enzyme concentrations when binding, ie. enzyme concentration should be in excess ");

    CLI::Option* cycopt = histsim.add_flag("--cyc", sp.cyclic, "histones in cyclic chain");
    histsim.add_option("--no", iNoOverride, "no override (possibbly not functional)",0);
    CLI::Option* setseed = histsim.add_option("--seed", sv.seed, "set seed for pseudo random number generator",0);
    CLI::Option* expandOpt = histsim.set_config("--expand","bash_parameters","reruns simmulation on current directory with outputmode 1, ie you have to cd to the actual simout folder",false);


    CLI11_PARSE(histsim, argc, argv);
    //if (! *writeStepsOpt )
    //    sp.writeTimeSteps = {};

    params::modvector.push_back("wildcard");  // make sure that zeroth element is reserved for rule wildcards
    params::modvector.push_back("un");  // make sure that the first element is "un" as it has a special meaning compared to real modifications

    // TODO all the "required" checks can probably be done by CLI11 , but I'm unsure how it done nicely
    if (! * expandOpt){
        if (! * stateFileOpt){
            std::cout << " state file required\n";
            exit(EXIT_FAILURE);
        }
        if(!* ruleFileOpt){
            std::cout << " rule file required\n";
            exit(EXIT_FAILURE);
        }
        if (! * paramFileOpt){
            std::cout << "parameter File required\n";
            exit(EXIT_FAILURE);
        }
    }
    else {
        stateFile = "statefile";
        ruleFile  = "rulefile";
        paramFile = "paramfile";
        sp.outputmode = 1;
        outputDir = "";
        sp.writeSteps = 0;
    }



    if(bCheck)
    {
        histoneMapFileOpt->required();
        if (! * ruleFileOpt || ! * stateFileOpt){
            if (! * paramFileOpt){
                std::cout << "parameter File required\n";
                exit(EXIT_FAILURE);
            }
            if (! * stateFileOpt){
                std::cout << " state file required \n";
                exit(EXIT_FAILURE);
            }
        }

        cParser XMLParser(sp.verbosity);

        XMLParser.mParseConfigFile(histoneMapFile);

        std::ofstream fsParseLogFilehandle;
        fsParseLogFilehandle.open("test.log");

        std::vector<std::vector<std::string> > vHistoneMap = XMLParser.mParseHistoneMap();

        std::cout << "States HM: \n";

        for(unsigned int i = 0; i < vHistoneMap.size(); i++)
        {
            for(unsigned int j = 0; j < vHistoneMap[i].size(); j++)
            {
                if(j == 0)
                    std::cout << vHistoneMap[i][0] << ": \n";
                else
                    std::cout << "\t" << vHistoneMap[i][j] << "\n";
            }
        }
        std::vector<std::vector<std::string> > vCheckHistones;
        if(!* ruleFileOpt && !* stateFileOpt)
            exit(EXIT_FAILURE);
        else if(* ruleFileOpt)
        {
            XMLParser.mParseConfigFile(ruleFile);

            std::vector<cEnzyme> vEnzymeSet;
            vEnzymeSet = XMLParser.mParseRuleFile(fsParseLogFilehandle);

            //		vCheckHistones = parseRuleOccurences(vEnzymeSet);  yes, i broke it because of file inclusions, would mean that the split.hpp needs to be seperated into independent header files
        }
        else if(* stateFileOpt)
        {
            std::string sSS = std::string(stateFile);

            XMLParser.mParseConfigFile(stateFile);
            std::string sStartingState = XMLParser.mParseStateFile();
            vCheckHistones = parseStateOccurences(sStartingState);
        }
        //if(sp.verbosity)
        //{
        std::cout << "States Check: \n";

        for(unsigned int i = 0; i < vCheckHistones.size(); i++)
        {
            for(unsigned int j = 0; j < vCheckHistones[i].size(); j++)
            {
                if(j == 0)
                    std::cout << vCheckHistones[i][0] << ": \n";
                else
                    std::cout << "\t" << vCheckHistones[i][j] << "\n";
            }
        }
        //}

        int iCheckHist = 0;
        int iCheckError = 0;

        for(unsigned int i = 0; i < vCheckHistones.size(); i++)	//compare sites
        {
            for(unsigned int j = 0; j < vHistoneMap.size(); j++)
            {
                if(vCheckHistones[i][0] == vHistoneMap[j][0])
                {
                    iCheckHist = 1;
                    break;
                }
            }
            if(iCheckHist == 0)
            {
                std::cout << "Histone " << vCheckHistones[i][0] << " from State does not exist in Histone Map!\n";
                iCheckError = 1;
            }
            else
                iCheckHist = 0;
        }

        int iCheckSite = 0;

        for(unsigned int i = 0; i < vCheckHistones.size(); i++)	//compare sites
        {
            for(unsigned int j = 0; j < vHistoneMap.size(); j++)
            {
                if(vCheckHistones[i][0] == vHistoneMap[j][0])
                {
                    for(unsigned int r = 1; r < vCheckHistones[i].size(); r++)	//compare sites
                    {
                        for(unsigned int s = 1; s < vHistoneMap[j].size(); s++)
                        {

                            if(vCheckHistones[i][r] == vHistoneMap[j][s])
                            {
                                iCheckSite = 1;
                                break;
                            }
                        }
                        if(iCheckSite == 0)
                        {
                            //if(sp.verbosity)
                            std::cout << "Site " << vCheckHistones[i][r] << " from State does not exist in Histone Map!\n";
                            iCheckError = 1;
                        }
                        else
                            iCheckSite = 0;
                    }
                }
            }
        }


        if(iCheckError == 0)
            exit(EXIT_SUCCESS);
        else
            exit(EXIT_FAILURE);
    }

    uint64 startTime = GetTimeMs64();

    //loop for runs

    if (sp.outputmode > 0)
        std::cout << "Performing " << sp.nRuns << " runs\n";

    std::string sNumRuns = std::to_string(sp.nRuns);
    std::vector<std::string> sOutPutDir;
    std::vector<std::string> sRun;
    if(*outputDirOpt) // might be unnecessary once using a fs library that understands what a path is
        outputDir = outputDir + "/";
    // make base folder for when in  0 outputmode:
    if (! * expandOpt){
        // clean start
        system(( "rm -r " + outputDir + "/*" ).c_str());
        if(!opendir(outputDir.c_str()))  {
            const int dir_err = mkdir(outputDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            if (-1 == dir_err)
            {
                std::cout << "Error creating base directory " << outputDir << "!\n";
                exit(EXIT_FAILURE);
            }
        }
    }

    for(uint iRun = 0; iRun < sp.nRuns; iRun++)
    {
        sRun.push_back(std::to_string(iRun));
        if (sp.outputmode !=0){
            std::string sFolderRun;

            if(sp.nRuns - sRun[iRun].length() == 0)
                sFolderRun = "0";
            else
                sFolderRun = std::string(sNumRuns.length() - sRun[iRun].length(), '0') + sRun[iRun];

            sOutPutDir.push_back( outputDir  + sFolderRun + "/");
            //sLogOutPutDir.push_back( sOutPutDir[iRun] + "logs");
            //sPropOutPutDir.push_back( sOutPutDir[iRun] + "prop_matrices");

            if(!opendir(sOutPutDir[iRun].c_str()))
                // i might have broken this , but i just hope that not
            {
                std::vector<std::string> vPathDirectories;
                std::string  sPath;
                // todo what do the followinng two lines do ??
                if  (!sOutPutDir[iRun].compare(0,1,"/"))
                    sPath = "/";
                vPathDirectories = explode ( sOutPutDir[iRun], "/");

                for(unsigned int i = 0; i < vPathDirectories.size(); i++)
                {
                    sPath = sPath  + vPathDirectories[i] + "/" ;
                    if(!opendir(sPath.c_str()))
                    {
                        const int dir_err = mkdir(sPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                        if (-1 == dir_err)
                        {
                            std::cout << "Error creating directory " << sPath << "!\n";
                            exit(EXIT_FAILURE);
                        }
                    }
                }
            }

            if(!opendir((sOutPutDir[iRun] + "logs").c_str()))
            {
                const int dir_err = mkdir((sOutPutDir[iRun] + "logs").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                if (-1 == dir_err)
                {
                    std::cout << "Error creating log directory " << sOutPutDir[iRun] + "logs" << "!\n";
                    exit(EXIT_FAILURE);
                }
            }
            if(!opendir((sOutPutDir[iRun] + "prop_matrices").c_str()))
            {
                const int dir_err = mkdir((sOutPutDir[iRun] + "prop_matrices").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                if (-1 == dir_err)
                {
                    std::cout << "Error creating prop directory " << sOutPutDir[iRun]  + "prop_matrices"<< "!\n";
                    exit(EXIT_FAILURE);
                }
            }


        }
    }


    // read config files only once
    Simparams paramfile_sp;

    //init parser

    std::ofstream fsSimLogFilehandle;
    std::ofstream fsParseLogFilehandle;
    sp.verbose = false;
    if(sp.verbosity)
    {
        sp.verbose = true;
        fsSimLogFilehandle.open(outputDir + "logs/sim.log");
        fsSimLogFilehandle << "Parsing config file... \n";
        fsParseLogFilehandle.open(outputDir + "logs/parse_hrf.log");
    }

    //parsing simulation parameters

    //parsing starting state

    cParser XMLParser(stateFile, sp.verbosity);

    sp.initstate= XMLParser.mParseStateFile();

    //Parsing rules
    XMLParser.mParseConfigFile(ruleFile);
    sp.enzymes = XMLParser.mParseRuleFile(fsParseLogFilehandle);
    sp.numEnzymes = sp.enzymes.size();
    // determine the maximal impact range of used enzymes
    sp.maxRuleReach = 0;
    std::vector <int > wildwrite ( params::sitevector.size(),0);
    for (uint ienz = 0; ienz < sp.numEnzymes; ++ienz) {
        for ( uint irule = 0; irule < sp.enzymes[ienz].rules.size(); irule++ )
        {
            sp.enzymes[ienz].rules[irule].intWrite = wildwrite;
            for (uint ihist = 0; ihist < sp.enzymes[ienz].rules[irule].target.subject.size(); ihist++)
            {
                for ( uint isite = 0 ; isite < params::sitevector.size(); isite++)
                {
                    if (sp.enzymes[ienz].rules[irule].target.subject[ihist].isite == isite)
                    {
                        int mod = sp.enzymes[ienz].rules[irule].write[ihist].imod;
                        sp.enzymes[ienz].rules[irule].intWrite[isite] =  mod ;
                    }

                }
            }
            // maxrulereach is the highest number of nucleosomes a rule is conscious of counted from the center
            // this is used to buffer the nucleosome arrays so that
            if (sp.maxRuleReach < sp.enzymes[ienz].rules[irule].target.left.size() )
                sp.maxRuleReach = sp.enzymes[ienz].rules[irule].target.left.size();
            if (sp.maxRuleReach < sp.enzymes[ienz].rules[irule].target.right.size() )
                sp.maxRuleReach = sp.enzymes[ienz].rules[irule].target.right.size();
            sp.buffer = 3*sp.maxRuleReach;
        }
    }


    if(* paramFileOpt)
    {
        XMLParser.mParseConfigFile(paramFile);
        paramfile_sp = XMLParser.mParseParamFile(fsParseLogFilehandle);
        // commandline arguments overwrite parameterfile options so check what is set
        if (!*SetIterationCountOpt)
            sp.SetIterationCount = paramfile_sp.SetIterationCount;
        if ( !*SetSimTimeOpt )
            sp.SetSimTime = paramfile_sp.SetSimTime;
        if (!*cycopt)
            sp.cyclic = paramfile_sp.cyclic;
        if (!*matrixlogOpt)
            sp.matrixlog = paramfile_sp.matrixlog;
        if (!*verbosityOpt)
            sp.verbosity	 = paramfile_sp.verbosity;
        //sp.stopcriterion = paramfile_sp.stopcriterion;
    }


    std::vector<std::string> vTmp = explode(sp.initstate,"}");
    if(sp.verbosity > 1)
    {
        fsParseLogFilehandle << "\tinitial state: " << sp.initstate << " \n";
        fsParseLogFilehandle << "\tnumber of nucleosomes: " << vTmp.size() << " \n";
    }

    sp.tdr = 0.3;
    sp.nNucleosomes = vTmp.size();
    if(sp.verbosity > 2)
        std::cout << "Simparams of configuration file: \n";

    sp.numRules = 0;
    for(uint i = 0; i < sp.enzymes.size(); ++i){
        sp.numRules = sp.numRules + sp.enzymes[i].rules.size();
        for (uint irule = 0; irule < sp.enzymes[i].rules.size(); irule++){
            if ( ! sp.enzymes[i].iIsActive){
                sp.enzymes[i].conc = 0;
            }
            sp.enzymes[i].rules[irule].rate_concentration = sp.enzymes[i].rules[irule].rate * sp.enzymes[i].conc;
        }
    }
    //starting logging


    XMLParser.mCloseFile();

    if(sp.verbose)
        fsSimLogFilehandle << "Create state data structure... \n";

    //copy input files into simout	TODO: in funktion auslagern
    if (! * expandOpt )
    {

        std::string sFilename;
        std::vector<std::string> vTMP;

        //std::cout << "Input files: " << stateFile << " - " << histoneMapFile << " - " << paramFile << " - " << ruleFile << "\n\n\n";

        boost::split(vTMP, stateFile,boost::is_any_of("/"));
        sFilename = vTMP[(vTMP.size()-1)];

        //sFilename = outputDir + "/" + sFilename;
        sFilename = outputDir +"statefile" ;

        std::ifstream source(stateFile, std::ios::binary);
        std::ofstream dest(sFilename, std::ios::binary);

        std::istreambuf_iterator<char> begin_source(source);
        std::istreambuf_iterator<char> end_source;
        std::ostreambuf_iterator<char> begin_dest(dest);
        std::copy(begin_source, end_source, begin_dest);

        source.close();
        dest.close();

        vTMP.clear();

        if(histoneMapFile != "")
        {
            boost::split(vTMP, histoneMapFile,boost::is_any_of("/"));
            sFilename = vTMP[(vTMP.size()-1)];

            //sFilename = outputDir + "/" + sFilename;
            sFilename = outputDir + "histoneMapFile";

            std::ifstream source2(histoneMapFile, std::ios::binary);
            std::ofstream dest2(sFilename, std::ios::binary);

            std::istreambuf_iterator<char> begin_source2(source2);
            std::istreambuf_iterator<char> end_source2;
            std::ostreambuf_iterator<char> begin_dest2(dest2);
            std::copy(begin_source2, end_source2, begin_dest2);

            source2.close();
            dest2.close();
        }

        vTMP.clear();
        boost::split(vTMP, paramFile,boost::is_any_of("/"));
        sFilename = vTMP[(vTMP.size()-1)];

        //sFilename = outputDir + "/" + sFilename;
        sFilename = outputDir + "paramfile";

        std::ifstream source3(paramFile, std::ios::binary);
        std::ofstream dest3(sFilename, std::ios::binary);

        std::istreambuf_iterator<char> begin_source3(source3);
        std::istreambuf_iterator<char> end_source3;
        std::ostreambuf_iterator<char> begin_dest3(dest3);
        std::copy(begin_source3, end_source3, begin_dest3);

        source3.close();
        dest3.close();

        vTMP.clear();
        boost::split(vTMP, ruleFile,boost::is_any_of("/"));
        sFilename = vTMP[(vTMP.size()-1)];

        //sFilename = outputDir + "/" + sFilename;
        sFilename = outputDir + "rulefile";

        std::ifstream source4(ruleFile, std::ios::binary);
        std::ofstream dest4(sFilename, std::ios::binary);

        std::istreambuf_iterator<char> begin_source4(source4);
        std::istreambuf_iterator<char> end_source4;
        std::ostreambuf_iterator<char> begin_dest4(dest4);
        std::copy(begin_source4, end_source4, begin_dest4);

        source4.close();
        dest4.close();
    }

    chromatin initialchromatin( sp);

    fsSimLogFilehandle.close();
    fsParseLogFilehandle.close();

    if(sp.verbosity > 2)
    {
        std::cout << "\nSimparams overwritten: \n";

        //for(auto elem : run_simparams)
        //{
        //	std::cout << elem.first << " : " << elem.second << "\n";
        //}
    }
    XMLParser.mParseConfigFile(ruleFile);
/*
    std::vector<cExRule> vExpressionRules = XMLParser.mParseExRules(fsParseLogFilehandle);

    if(sp.verbosity > 2)
    {

        for(unsigned int k = 0; k < vExpressionRules.size(); k++)
        {
            std::cout  << "\nRegion: " << vExpressionRules[k].sRegionName << "\n";
            std::cout  << "Start: " << vExpressionRules[k].iRegionStart << "\n";
            std::cout  << "End: " << vExpressionRules[k].iRegionEnd << "\n";
            std::cout  << "Mod: " << vExpressionRules[k].sCheckMod << "\n";
        }
    }
*/
    sp.iNumChannelGroups = 2 * sp.numRules;


    sp.numEnzymes = sp.enzymes.size();


    //Indexing of which channel belongs to which Enzyme (2 channels per rule)

    uint iChannel = 0;

    for (uint  i = 0; i< sp.numEnzymes; i++)
    {
        if(sp.verbosity > 1)
            fsSimLogFilehandle << "Enzyme: " << i << "\n";
        sp.vChannelTransl.push_back("");
        for (uint j = 0; j < 2* sp.enzymes[i].rules.size(); j++)
        {
            if(sp.verbosity > 1)
                fsSimLogFilehandle << "Channel: " << iChannel << "\n";

            //mChannelTransl[i][j] = iChannel;
            if(sp.verbose)
                sp.vChannelTransl[i] = sp.vChannelTransl[i] + "_" + std::to_string(iChannel);
            iChannel++;

            sp.c2e.push_back( i );
        }
    }

    if(sp.verbose)
        printVectorToFile(sp.vChannelTransl, fsSimLogFilehandle);

    int iNumRules;

    for (uint i = 0; i < sp.numEnzymes; i++)
    {
        iNumRules = 0;

        for (uint j = 0; j < 2* sp.enzymes[i].rules.size(); j++)
        {

            sp.c2r.push_back( iNumRules);
            if (j % 2 == 1)
            {
                iNumRules++;
            }
        }
    }
    uint ruleSize = (2 *  sp.maxRuleReach + 1 ) * params::sitevector.size();
    std::vector< uint > tmpIntstring(ruleSize,0);
    for (uint i = 0; i < sp.numEnzymes; i++){
        for (uint j = 0; j < sp.enzymes[i].rules.size(); j++){
            // initialize char targetempty
            sp.enzymes[i].rules[j].target.intString = tmpIntstring;
            //strcpy(sp.enzymes[i].rules[j].target.intString, std::string(size, 0).c_str() );
            //left side
            for (uint inuc = 0; inuc <  sp.enzymes[i].rules[j].target.left.size() ;inuc++ ){
                for (uint isite = 0; isite	< params::sitevector.size() ; isite++){
                    for (uint ihist = 0; ihist < sp.enzymes[i].rules[j].target.left[inuc].size()  ;ihist++){
                        if (sp.enzymes[i].rules[j].target.left[inuc][ihist].isite == isite ){
                            // intstring has fixed size 2*maxrulereach +1, rule target.left may be shorter thus buffer
                            uint buffer = inuc + sp.maxRuleReach - sp.enzymes[i].rules[j].target.left.size();
                            sp.enzymes[i].rules[j].target.intString[(inuc + buffer) * params::sitevector.size() + isite] = sp. enzymes[i].rules[j].target.left[inuc][ihist].imod;
                        }
                    }
                }
            }
            // subject
            for (uint isite = 0; isite	< params::sitevector.size() ; isite++){
                for (int ihist = 0; ihist < sp.enzymes[i].rules[j].target.subject.size()  ;ihist++){
                    if (sp.enzymes[i].rules[j].target.subject[ihist].isite == isite )
                        sp.enzymes[i].rules[j].target.intString[
                                ( sp.maxRuleReach )* params::sitevector.size() + isite]
                                = sp.enzymes[i].rules[j].target.subject[ihist].imod;
                }
            }
            //right side
            for (uint inuc = 0; inuc <  sp.enzymes[i].rules[j].target.right.size() ;inuc++ ){
                for (uint isite = 0; isite	< params::sitevector.size() ; isite++){
                    for (int ihist = 0; ihist < sp.enzymes[i].rules[j].target.right[inuc].size()  ;ihist++){
                        if (sp.enzymes[i].rules[j].target.right[inuc][ihist].isite == isite )
                            sp.enzymes[i].rules[j].target.intString[ ( sp.maxRuleReach  + 1 + inuc ) * params::sitevector.size() + isite] =  sp.enzymes[i].rules[j].target.right[inuc][ihist].imod;
                    }
                }
            }



        }
    }

    std::vector<bool> btmpvec;
    for(uint i = 0; i < sp.iNumChannelGroups; i++)
    {
        sp.zerovec.push_back(0);
        sv.bmReactionRates.push_back(btmpvec);
        sv.channelSiteCount.push_back(0);
        sv.vRowSums.push_back(0);
        if (i % 2 == 0)
            if (sp.nonDynamiConcentrations)
                sv.channelRates.push_back(   sp.enzymes[sp.c2e[i]].rules[sp.c2r[i]].rate);
            else
                sv.channelRates.push_back(   sp.enzymes[sp.c2e[i]].rules[sp.c2r[i]].rate_concentration);
        else
            sv.channelRates.push_back( sp.enzymes[sp.c2e[i]].rules[sp.c2r[i]].dissociationRate);
        // front and end  buffered array
        for(uint j = 0; j < sp.nNucleosomes + 2 * sp.buffer; j++)
            sv.bmReactionRates[i].push_back(0);
    }
    // front buffer
    for ( uint i =0; i< sp.buffer ;i++)
        sv.boundEnzymes.push_back(1);

    for(uint j = 0; j < sp.nNucleosomes; j++)
    {
        sv.vColSums.push_back(0);
        sv.boundEnzymes.push_back(0);
        sv.ratesReaction.push_back(btmpvec);
        for(uint i = 0; i < sp.iNumChannelGroups; i++)
            sv.ratesReaction[j].push_back(0);
    }
    //back buffer
    for ( uint i =0; i< sp.buffer ;i++)
        sv.boundEnzymes.push_back(1);
    sv.prePropSum = 0;

    std::ofstream outfile_short;
    outfile_short.open(outputDir + "outfile_short.txt");
    std::cout << "opened short outfile at " << outputDir << "outfile_short.txt\n";
    std::ofstream channels;
    channels.open(outputDir + "/channels.txt");
    std::ofstream bash_parameters;
    bash_parameters.open(outputDir + "/bash_parameters");

    std::vector<chromatin> finalchromatins;

    chromatin chrtmp(sp);
    for (uint iRun = 0; iRun < sp.nRuns; iRun++) {
        finalchromatins.push_back(chrtmp);
    }
    // one seed per simulation, anyone proove please that this reduces the quality of the random numbers, we're not doing cryptography
    if ( ! *setseed )
    {
        std::random_device rd;
        srand(GetTimeMs64());
        sv.seed = rd();
    }

    bash_parameters << "seed=" + std::to_string( sv.seed)+"\n";
    bash_parameters << "rf=rulefile\n";
    bash_parameters << "sf=statefile\n";
    bash_parameters << "pf=paramfile\n";
    bash_parameters << "cyc=" + std::to_string( sp.cyclic)+"\n";
    bash_parameters << "nondyn=" + std::to_string( sp.nonDynamiConcentrations)+"\n";
    bash_parameters << "runs=" + std::to_string( sp.nRuns)+"\n";
    bash_parameters << "tdr=" + std::to_string( sp.tdr)+"\n";
    //bash_parameters << "s=" +  sp.initstate+"\n";
    //bash_parameters << "i=" + std::to_string( sp.SetIterationCount)+"\n";
    //bash_parameters << "t=" + std::to_string( sp.SetSimTime)+"\n";

    // initialize modmatrix to use printIntString
    std::vector<std::string> tmpstrvec;
    uint modcounter = 0;
    for (uint isite = 0 ; isite < params::sitevector.size(); isite++)
    {
        params::modmatrix.push_back(tmpstrvec);
        for (uint imod = 0; imod < params::modvector.size(); ++imod) {
            params::modmatrix[isite].push_back(std::to_string(modcounter));
            modcounter++;
        }
    }
    sv.init(sp);


    //outfile_short << "# hash: " << sp.hash << " \n";
    outfile_short << "# seed: " << sv.seed << " \n";
    std::string headerline = "";
    // Legend of what number means which modification
    for (uint site =0 ; site < params::sitevector.size() ; site ++)
    {
        for (uint modi = 1; modi < params::modvector.size() ; ++modi) {
            outfile_short <<"#  " + params::modmatrix[site][modi] + ": " + params::sitevector[ site ]+ "." + params::modvector[ modi ] + "]\n" ;
            headerline.append(params::sitevector[ site ]+ "." + params::modvector[ modi ] + "],\t ");
        }
    }
    // table header
    outfile_short << "\n# sequence,\t" + headerline + "run,\titeration,\ttime \n";

    outfile_short.close();
    // write channel number and channel function table
    for (uint channel = 0 ; channel < sp.c2e.size(); channel++ ){
        channels << channel<< ": " << sp.enzymes[sp.c2e[channel]].rules[sp.c2r[channel]].name<< "\n";
    }
    channels.close();

    for(uint iRun = 0; iRun < sp.nRuns; iRun++)
    {
        std::string runOutPutDir = outputDir; // in case of outputmode 0 ...
        //ability to continue?
        if (sp.outputmode !=0){
            if(opendir(sOutPutDir[iRun].c_str()) && iNoOverride != 0)
                continue;
            std::cout << sOutPutDir[iRun] +"logs" << "\n";
            std::cout << "Running simulation " << std::to_string(iRun) << "\n";
            runOutPutDir = sOutPutDir[iRun];
        }

        Simvars runsimvars= sv; // make a copy of the simparams so that we can change them and still track them
        runsimvars.seed = sv.seed * (iRun + 1);
        finalchromatins[iRun] =  runGillespie_exp(initialchromatin, sp , runsimvars, runOutPutDir);
    }

    //std::cout << "\nProgram terminated!\n";

    //if (sp.outputmode > 0)
        std::cout << "Total execution time: " << GetTimeMs64() - startTime << " ms\n";

    return 0;

}
