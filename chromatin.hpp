/*
 * chromatin.hpp
 *
 *  Created on: Apr 23, 2015
 *      Author: nicoh
 */



#ifndef CHROMATIN_HPP_
#define CHROMATIN_HPP_

#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>
#include "nucleosome.hpp"


class chromatin
{
	public:
        chromatin(Simparams simparams);
		virtual ~chromatin();
        std::vector<cNucleosome> mInitNucleosomes(Simparams &simparams);
        std::vector<uint> initNucleosomesString(Simparams& simparams, std::vector<cNucleosome> aNucleosomes);
        void mPrintStateToFile(std::ofstream* fsFilehandle);
        std::vector<uint > nucleosomestring;
        std::string print_nuc(Simparams& sp, uint inuc);
        std::string print_short (Simparams& sp);
        std::string print_diff (Simparams& sp);
        std::string print_counts (Simparams& sp);

	//private:
		int iNumNucleosomes;
        std::vector<cNucleosome> aNucleosomes; //pointers of nucleosome objects

		std::vector<int> aOrigins;
};

#endif /* CHROMATIN_HPP_ */
