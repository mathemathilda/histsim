/*
 * Parser.cpp
 *
 *  Created on: Sep 10, 2015
 *      Author: nicoh
 */

#include "Parser.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include "split.hpp"


cParser::cParser(char* cFilename, int iVerbosity_)
{

	iVerbosity = iVerbosity_;

	std::ifstream XMLFile(cFilename);

	//LogHandle = LogHandle_;

	//if(iVerbosity > 1)
	//	fsParseLogFilehandle = LogHandle;


	//if(iVerbosity > 2)
	//	fsDebugLogFilehandle.open("logs/parse_dbg.log");



	std::vector<char> temp((std::istreambuf_iterator<char>(XMLFile)), std::istreambuf_iterator<char>());

	buffer = temp;
	buffer.push_back('\0');

	std::string sBuff(buffer.data());

	//std::cout << sBuff;

	doc.parse<0>(&buffer[0]);

	//if(iVerbosity > 0)
	//	LogHandle << "Parsing config file... \n";

	std::cout << "Parsing config file... \n";

	//misses rapidxml::file standard constructor, solution?
	//fileXML = rapidxml::file<>(cFilename);
	//doc.parse<0>(fileXML.data());


}

cParser::~cParser()
{
	fsDebugLogFilehandle.close();
	fsParseLogFilehandle.close();
}

void cParser::mCloseFile()
{
	fsDebugLogFilehandle.close();
	fsParseLogFilehandle.close();
}

std::map< std::string, boost::variant<std::string, int> > cParser::mParseSimParams()
{

	fsParseLogFilehandle << "\nSimparams: \n \n";


	std::map< std::string, boost::variant<std::string, int> > simparams;

	simparams["SetSimTime"] = 0;
	simparams["SetIterationCount"] =0;
	rapidxml::xml_node<> *node =  doc.first_node("cyclicChromosomes");

	rapidxml::xml_attribute<> *attr;
	//std::cout << "\nGetting cyclic info: \n \n ";

	if(node)
	{
		attr = node->first_attribute("value");
		if(attr->value() == "")
		{
			std::cout << "Cyclic chromosomes not defined! \n";
			exit(1);
		}
		simparams["cyclic"] = attr->value();
		if(iVerbosity > 1)
			fsParseLogFilehandle << "\tcyclic nucleosomes: " << simparams["cyclic"] << " \n";


	}
	else
	{
		std::cout << "Cyclic chromosomes not defined! \n";
		std::cout << "Error parsing cyclic chain info!";

		exit(1);
	}


	node = doc.first_node("simulationTime");

			//std::cout << "\nGetting cyclic info: \n \n ";

	if(node)
	{
		attr = node->first_attribute("value");
		if(attr->value() == "")
		{
			std::cout << "simulationTime not defined! \n";
			exit(1);
		}
		simparams["simulationTime"] = attr->value();
		if(iVerbosity > 1)
			fsParseLogFilehandle << "\tsimulationTime: " << simparams["simulationTime"] << " \n";


	}
	else
	{
		std::cout << "simulationTime not defined! \n";
		std::cout << "Error Simulation Time!";
		exit(1);
	}


	node = doc.first_node("initialState");


	if(node)
	{
		simparams["initstate"] = node->first_attribute("value")->value();   //BUG!

		if(attr->value() == "")
		{
			std::cout << "Initial State not defined! \n";
			exit(1);
		}

		if(iVerbosity > 1)
			fsParseLogFilehandle << "\tinitial state: " << simparams["initstate"] << " \n";

		std::string sTmp = boost::lexical_cast<std::string>(simparams["initstate"]);

		std::vector<std::string> vTmp = explode(sTmp,"}");
		if(iVerbosity > 1)
			fsParseLogFilehandle << "\tnumber of nucleosomes: " << vTmp.size() << " \n";

		simparams["nNucleosomes"] = vTmp.size();
	}
	else
	{
			std::cout << "Initial State not defined! \n";
			std::cout << "Error parsing initial State!";

			exit(1);
	}

	//TODO: numReplications, replicationmodel, stopcriterion, ....


	return simparams;
}



std::map< std::string, boost::variant<std::string, int> > cParser::mParseSimParams2(std::ofstream& LogHandle)
{

	std::cout << "\nSimparams: \n \n";


	std::map< std::string, boost::variant<std::string, int> > simparams;

    simparams["SetSimTime"] = 0;
    simparams["SetIterationCount"] = 0;
    simparams["cyclic"] = 0;
    simparams["initialState"] = 0;
    simparams["initstate"] = 0;
    simparams["stopcriterion"] = -1;


    simparams["hash"] = 0;

    rapidxml::xml_node<> *node = doc.first_node("simulation");
	rapidxml::xml_attribute<> *attr;

	std::vector<std::string> vTmp;
	std::vector<std::string> vTmp2;
	std::vector<std::string> vTmp3;
	std::string sTmp;
	if(node)
	{
        rapidxml::xml_node<> *oInitStateNode = node->first_node("initialState");
        if(oInitStateNode)
        {
            simparams["initstate"] = oInitStateNode->first_attribute("value")->value();   //BUG!
    		attr = oInitStateNode->first_attribute("value");

            if(attr->value() == "")
            {
                std::cout << "Initial State not defined! \n";

                exit(1);
            }

            if(iVerbosity > 1)
            	std::cout << "\tinitial state: " << simparams["initstate"] << " \n";

            std::string sTmp = boost::lexical_cast<std::string>(simparams["initstate"]);

            std::vector<std::string> vTmp = explode(sTmp,"}");
            if(iVerbosity > 1)
            	std::cout << "\tnumber of nucleosomes: " << vTmp.size() << " \n";

            simparams["nNucleosomes"] = vTmp.size();

        }
        else
        {
            std::cout << "Initial State not defined! \n";
			exit(1);
        }

        rapidxml::xml_node<> *oSimTimeNode = node->first_node("simulationTime");
        if(oSimTimeNode)
        {
            attr = oSimTimeNode->first_attribute("value");
            if(attr->value() == "")
            {
                std::cout << "simulationTime not defined! \n";
                simparams["stopcriterion"] = 0;

                exit(1);
            }
            simparams["SetSimTime"] = attr->value();
            simparams["stopcriterion"] = 1;

        }
        else
        {
            std::cout << "simulationTime not defined! \n";
        }

        rapidxml::xml_node<> *oCyclicNode = node->first_node("cyclicChromosomes");
        if(oCyclicNode)
        {
            attr = oCyclicNode->first_attribute("value");
            if(attr->value() == "")
            {
                std::cout << "Cyclic chromosomes not defined! \n";
                exit(1);
            }
            simparams["cyclic"] = attr->value();
        }
        else
        {
            std::cout << "Cyclic chromosomes not defined! \n";
            exit(1);
        }

        rapidxml::xml_node<> *oHashNode = node->first_node("configurationHashKey");
        if(oHashNode)
        {
            attr = oHashNode->first_attribute("value");
            if(attr->value() == "")
            {
                std::cout << "Hash not defined! \n";
            }
            simparams["hash"] = attr->value();
        }
        else
        {
            std::cout << "Hash not defined! \n";
        }

	}



    return simparams;
}



std::vector<cEnzyme*> cParser::mParseEnzymeSet(std::ofstream& LogHandle)
{

	LogHandle << "\nEnzymes: \n \n";


	std::vector<cEnzyme*> vEnzymeSet;
	rapidxml::xml_node<> *rootnode = doc.first_node("simulation");

	//rapidxml::xml_node<> *node = doc.first_node("enzymeSet");
	rapidxml::xml_node<> *node = rootnode->first_node("enzymeSet");
	rapidxml::xml_attribute<> *attr;

	std::vector<std::string> vTmp;
	std::vector<std::string> vTmp2;
	std::vector<std::string> vTmp3;
	std::string sTmp;
	if(node)
	{
		int enzymecounter = 0;


		for(rapidxml::xml_node<> *enzymeDef = node->first_node("enzymeDef"); enzymeDef != 0; enzymeDef = enzymeDef->next_sibling())
		{

			if(strcmp(enzymeDef->name(), "enzymeDef")!=0)
			        continue;

			cEnzyme* newEnzyme = new cEnzyme;
			if(iVerbosity > 1)
				std::cout << "Enzyme " << enzymecounter << ":  \n";


			for (rapidxml::xml_node<> *enzymeparam = enzymeDef->first_node(); enzymeparam; enzymeparam = enzymeparam->next_sibling())
			{

				//atof strtof ???

				const char* cnodename = enzymeparam->name();
				std::string nodename(cnodename);

				if(nodename == "name")
				{
					std::string sEnzName = enzymeparam->first_attribute("value")->value();
					if(sEnzName.empty())	//check for non existent missing
					{
						std::cout << "Enzyme name not defined! \n";
						exit(1);
					}
					else
					{
						newEnzyme->sName = enzymeparam->first_attribute("value")->value();
					}

					/*std::string sEnzEnab = enzymeparam->first_attribute("enabled")->value();
					if(sEnzEnab.empty())
					{
						std::cout << "Enzyme enable status not defined! \n";
						exit(1);
					}
					else
					{
						newEnzyme->iIsActive = atoi(enzymeparam->first_attribute("enabled")->value());
					}
					*/
					newEnzyme->iIsActive = 1;	//remove completely later on

					if(iVerbosity > 1)
						std::cout << "\tName: " << newEnzyme->sName << " \n";

				}
				else if(nodename == "size")
				{
					std::string sEnzSize = enzymeparam->first_attribute("value")->value();
					if(sEnzSize.empty())
					{
						std::cout << "Enzyme size not defined! \n";
						exit(1);
					}
					else
					{
						int iEnzSize = atoi(enzymeparam->first_attribute("value")->value());
						newEnzyme->iBindingSize = iEnzSize;
					}
					if(iVerbosity > 1)
						std::cout << "\tSize: " << newEnzyme->iBindingSize << " \n";
				}
				else if(nodename == "concentration")
				{
					std::string sEnzConc = enzymeparam->first_attribute("value")->value();
					if(sEnzConc.empty())
					{
						std::cout << "Enzyme concentration not defined! \n";
						exit(1);
					}
					else
					{
						float fEnzConc = atof(enzymeparam->first_attribute("value")->value());
						newEnzyme->fConcentration = fEnzConc; //TODO: type unhandled
					}
					if(iVerbosity > 1)
						std::cout << "\tConcentration: " << newEnzyme->fConcentration << " \n";
				}
				else if(nodename == "bindingRatesRuleSet")
				{



					if(iVerbosity > 1)
						std::cout << "\tRule: \n";



					rule* pNewRule = new rule;

					pNewRule->parent = enzymecounter;

					for (rapidxml::xml_node<> *ruleparam = enzymeparam->first_node(); ruleparam; ruleparam = ruleparam->next_sibling())
					{
						const char* crulenodename = ruleparam->name();
						std::string rulenodename(crulenodename);

						if(rulenodename == "name")
						{
							std::string sRuleName = ruleparam->first_attribute("value")->value();
							if(sRuleName.empty())
							{
								std::cout << "Rule name not defined! \n";
								exit(1);
							}
							else
							{
								pNewRule->name = ruleparam->first_attribute("value")->value();
							}
							if(iVerbosity > 1)
								std::cout << "\t\tName: " << pNewRule->name << " \n";
						}
						//else if(rulenodename == "type")
						//{
						//	if(ruleparam->first_attribute("value")->value() == "")
						//	{
						//		std::cout << "Rule type not defined! \n";
						//		exit(1);
						//	}

						//	pNewRule->type = ruleparam->first_attribute("value")->value();
							//std::cout << "Size: " << newEnzyme->iBindingSize << " \n";
						//}
						else if(rulenodename == "target")
						{

							if(ruleparam->first_attribute("value")->value() == "")
							{
								std::cout << "Rule matching target not defined! \n";
								exit(1);
							}

							pNewRule->target = ruleparam->first_attribute("value")->value();
							if(iVerbosity > 1)
								std::cout << "\t\tTarget: " << pNewRule->target << " \n";
							//pNewRule->vTargetVec = mParseMatchingPattern(pNewRule->target);

							pNewRule->vTargetVec = decomposeTarget(ruleparam->first_attribute("value")->value());
						}
						else if(rulenodename == "rule")
						{

							if(ruleparam->first_attribute("value")->value() == "")
							{
								std::cout << "Rule rewrite string not defined! \n";
								exit(1);
							}

							pNewRule->srule = ruleparam->first_attribute("value")->value();

							if(iVerbosity > 1)
								std::cout << "\t\tRewrite to: " << pNewRule->srule << " \n";


							vTmp = decomposeTarget(ruleparam->first_attribute("value")->value());

							pNewRule->vRewriteVec.push_back(vTmp[1]);
							//std::cout << pNewRule->vRewriteVec[0] << "\n";

							//pNewRule->vRewriteVec.push_back(pNewRule->srule.substr(1,pNewRule->srule.length()-2));
							//pNewRule->vRewriteVec = mParseMatchingPattern(pNewRule->rule);
						}
						else if(rulenodename == "rate")
						{

							if(ruleparam->first_attribute("value")->value() == "")
							{
								std::cout << "Rule rate not defined! \n";
								exit(1);
							}

							pNewRule->rate = boost::lexical_cast<float>(ruleparam->first_attribute("value")->value());

							if(iVerbosity > 1)
								std::cout << "\t\tRate: " << pNewRule->rate << " \n";
						}
						else if(rulenodename == "enabled")
						{

							if(ruleparam->first_attribute("value")->value() == "")
							{
								std::cout << "Rule enabling not defined! \n";
								exit(1);
							}

							pNewRule->enabled = boost::lexical_cast<int>(ruleparam->first_attribute("value")->value());
							if(iVerbosity > 1)
								std::cout << "\t\tEnabled: " << pNewRule->enabled << " \n";
						}
						else if(rulenodename == "dissociationRate")
						{

							if(ruleparam->first_attribute("value")->value() == "")
							{
								std::cout << "Rule dissociation rate not defined! \n";
								exit(1);
							}

							//newEnzyme->fDissociationRate = boost::lexical_cast<float>(enzymeparam->first_attribute("value")->value()); //TODO: type unhandled
							pNewRule->fDissociationRate = boost::lexical_cast<float>(ruleparam->first_attribute("value")->value());
							//std::cout << "dissocrate: " << pNewRule->fDissociationRate << "\n";
							if(iVerbosity > 1)
								std::cout << "\t\tDissociation rate: " << pNewRule->fDissociationRate << " \n";
						}

					}

					if(pNewRule->enabled == 1)
					{
						newEnzyme->mAddRule(pNewRule);
						if(iVerbosity > 2)
							fsDebugLogFilehandle << newEnzyme->sName << ";" << newEnzyme->fConcentration << ";" << pNewRule->target << "->" << pNewRule->srule << ";" << pNewRule->rate << ";" << pNewRule->fDissociationRate << "\n";
					}
				}
			}

			vEnzymeSet.push_back(newEnzyme);

			enzymecounter++;
		}

	}
	else
	{
		std::cout << "Enzyme set not defined! \n";
		exit(1);
	}


	return vEnzymeSet;
}


std::vector<std::string> cParser::mParseMatchingPattern(std::string sNuc)
{
	std::vector<std::string> vHistoneVector;

	std::vector<std::string> vLeftSide;
	std::vector<std::string> vRightSide;

	std::vector<std::string> vTemp;
	std::vector<std::string> vOut;

	boost::split(vLeftSide, sNuc, boost::is_any_of("("));

	boost::split(vRightSide, vLeftSide[1], boost::is_any_of(")"));

	//std::cout << sNuc << " splitted into ";
	//std::cout << vLeftSide[0] << " and ";		//left
	//std::cout << vRightSide[0] << " and ";		//middle (target)
	//std::cout << vRightSide[1] << "\n";			//right



	//decompose left side into single nucleosomes

	boost::split(vTemp, vLeftSide[0], boost::is_any_of("}"));

	//std::cout << "Left side nucleosomes: \n";

	if(vLeftSide[0] == "")
	{
		//std::cout << "No left side!\n";
		vOut.push_back("x");
	}
	else
	{
		for(unsigned int i = 0; i < vTemp.size()-1; i++)
		{
			vTemp[i] = vTemp[i].substr(1, vTemp[i].size() - 1);


			vOut.push_back(vTemp[i]);

			//std::cout << i << ": " << vTemp[i] << "\n";
		}
	}

	vOut.push_back("-");



	//std::cout << "Matching target nucleosome: \n";

	if(vRightSide[0] == "")
	{
		//std::cout << "Error! No matching Target!\n";
		exit(1);
	}
	else
	{
		vRightSide[0] = vRightSide[0].substr(1, vRightSide[0].size() - 2);



		vOut.push_back(vRightSide[0]);

		//std::cout << vRightSide[0] << "\n";

	}

	vOut.push_back("-");

	boost::split(vTemp, vRightSide[1], boost::is_any_of("}"));

	//std::cout << "Right side nucleosomes: \n";

	if(vRightSide[1] == "")
	{
		//std::cout << "No right side!\n";
		vOut.push_back("x");
	}
	else
	{
		for(unsigned int i = 0; i < vTemp.size()-1; i++)
		{
			vTemp[i] = vTemp[i].substr(1, vTemp[i].size() - 1);


			vOut.push_back(vTemp[i]);

			//std::cout << i << ": " << vTemp[i] << "\n";
		}
	}


	return vOut;
}



std::map< int, std::vector<std::string> > cParser::mParseNucleosome(std::vector<std::string> vNuc)
{
	std::map< int, std::vector<std::string> > mHistoneMap;


	for(unsigned int i = 0; i < vNuc.size(); i++)
	{

		if(vNuc[i] == "x")
		{


		}
		std::vector<std::string> vTemp;

		boost::split(vTemp, vNuc[i], boost::is_any_of("]"));

		for(unsigned int j; j< vTemp.size(); j++)
		{
			//mHistoneMap[i][j]

		}

	}

	return mHistoneMap;
}

int cParser::checkSyntax(std::string sSubject, int iMode)
{
	int curly_brackets = 0;
	int parenthesis = 0;
	int square_brackets = 0;
	int dot = 0;

	for (int i = 0;i < sSubject.size(); i++)
	{
		//echo $i." - ".$teststring[$i]."<br>";

		if(sSubject[i] == '{')
		{
			if(square_brackets != 0)
			{
				std::cout << "Syntax Error! Closing ] missing!";
				return 0;
			}
			curly_brackets++;
		}
		if(sSubject[i] == '}')
		{
			curly_brackets--;
			if(square_brackets != 0)
			{
				std::cout << "Syntax Error! Closing ] missing!";
				return 0;
			}
		}
		if(sSubject[i] == '(')
			parenthesis++;
		if(sSubject[i] == ')')
			parenthesis--;

		if(sSubject[i] == '[')
		{
			if(curly_brackets != 1)
			{
				std::cout << "Syntax Error! [ outside of nucleosome or opening { missing!";
				return 0;
			}
			square_brackets++;
		}
		if(sSubject[i] == ']')
		{
			if(curly_brackets != 1)
			{
				std::cout << "Syntax Error! ] outside of nucleosome or opening { missing!";
				return 0;
			}
			dot = 0;
			square_brackets--;
		}
		if(sSubject[i] == '.' && curly_brackets != 1 && square_brackets != 1)
		{
			std::cout << "Syntax Error! . outside of histone definition or opening [ missing!";
			return 0;
		}
		if(sSubject[i] == '.')
		{
			dot++;
		}
		if(dot > 1)
		{
			std::cout << "Syntax Error! Multiple . in histone definition!";
			return 0;
		}
	}

	if(curly_brackets != 0)
	{
		std::cout << "Syntax Error! Missing opening or closing curly bracket!";
		return 0;
	}
	if(parenthesis != 0)
	{
		std::cout << "Syntax Error! Missing opening or closing parenthesis!";
		return 0;
	}
	if(square_brackets != 0)
	{
		std::cout << "Syntax Error! Missing opening or closing square bracket!";
		return 0;
	}


	return 1;
}
