/*
 * rule.hpp
 *
 *  Created on: May 21, 2015
 *      Author: nicoh
 */

#ifndef RULE_HPP_
#define RULE_HPP_

#include <string>
#include <vector>
//struct or class?
#include "split.hpp"

class Target
{
public:

        std::vector<std::vector<Hist>> left;
        std::vector<std::vector<Hist> > right;
        std::vector<Hist> subject;
        std::vector	<uint> intString;
};
Target string_to_target(std::string targetstring_);
std::vector< std::vector<Hist>> subPattern_to_subtarget( std::string subPatternString);
class Rule
{
    public:
        std::string name;
        bool enabled;
        float rate;
        float rate_concentration;
        float dissociationRate;
        Target target;
        std::vector< Hist > write;
        std::vector < int> intWrite;

};


#endif /* RULE_HPP_ */
