CC=g++
CFLAGS=-c -O2 -Wall -fopenmp -std=c++0x
LDFLAGS= -fopenmp
SOURCES=cEnzyme.cpp params.cpp cExRule.cpp chromatin.cpp cXMLParser.cpp gillespie.cpp helper.cpp histone.cpp HistSim.cpp nucleosome.cpp rule.cpp split.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=HistSim

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *o
