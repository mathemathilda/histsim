/*
 * cExRule.hpp
 *
 *  Created on: Mar 29, 2017
 *      Author: nicoh
 */

#ifndef CEXRULE_HPP_
#define CEXRULE_HPP_

#include <string>


class cExRule {
public:
	cExRule(int iStart_, int iEnd_, std::string sName_, std::string sCheckMod_, std::string sCheckType_, float fCheckAmount_, std::string sEffectType_, float fEffectValue_, float fEffectDelay_);
	cExRule();
	virtual ~cExRule();

	int iRegionStart;
	int iRegionEnd;
	std::string sRegionName;
	std::string sCheckMod;
	std::string sCheckType;
	float fCheckAmount;
	std::string sEffectType;
	float fEffectValue;
	float fEffectDelay;

};

#endif /* CEXRULE_HPP_ */
