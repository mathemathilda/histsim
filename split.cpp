#include "split.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "histone.hpp"
#include <algorithm>
#include "cEnzyme.hpp"

std::vector<std::string> explode(std::string input, std::string delimiter)
{
	std::vector<std::string> result;

	std::string tempstring;

	std::size_t found = input.find_first_of(delimiter);
	std::size_t found_before = 0;

	while (found!=std::string::npos)
	{
	  	///std::cout << found_before << '\n';
	  	//std::cout << found << '\n';


	  	tempstring = input.substr(found_before,found-found_before);

  		if(tempstring.length() == 0)
  		{
  			found=input.find_first_of(delimiter,found+1);
  			continue;
  		}
	  	if(tempstring.at(0) == delimiter[0])
	  		tempstring = tempstring.substr(1, tempstring.length()-1);

		result.push_back(tempstring);

	  	//std::cout << tempstring << '\n';

		//teststring[found]='*';
		found_before = found;
		found=input.find_first_of(delimiter,found+1);

		if(found == std::string::npos)
		{
			tempstring = input.substr(found_before, input.length()-found_before);

			if(tempstring.at(0) == delimiter[0])
				tempstring = tempstring.substr(1, tempstring.length()-1);

			if(tempstring != "")
				result.push_back(tempstring);


		}
	}

	return result;
}

std::vector<std::string> decomposeTarget(std::string input)
{
	std::vector<std::string> result;

	std::string tempstring;

	std::size_t found = input.find_first_of("(");
	std::size_t found_before = 0;

	tempstring = input.substr(found_before,found-found_before);

	if(tempstring.length() == 0)
		result.push_back("");
	else
		result.push_back(tempstring);
	found_before = found;

	found = input.find_first_of(")",found+1);

	tempstring = input.substr(found_before,found-found_before);

	if(tempstring.length() == 0)
		result.push_back("");
	else
	{
		if(tempstring[0] == '(')
			tempstring = tempstring.substr(1, tempstring.length()-1);

		if(tempstring.back() == ')')
			tempstring = tempstring.substr(0, tempstring.length()-1);

        if(tempstring[0] == '{')
            tempstring = tempstring.substr(1, tempstring.length()-1);

        if(tempstring.back() == '}')
            tempstring = tempstring.substr(0, tempstring.length()-1);

		result.push_back(tempstring);
	}
	tempstring = input.substr(found+1, input.length()-found);

	if(tempstring.length() == 0)
		result.push_back("");

	else
		result.push_back(tempstring);

	return result;
}

bool mNucMatch(std::vector<Hist>& vNucleosomeHist, std::vector<Hist>& vMatchingHist)
{
    uint iChecksum = 0;
    int result;

	for(int i =0; i < vMatchingHist.size(); i++)
	{
        result = getHistValue(vNucleosomeHist, vMatchingHist[i].isite);

        if(result >= 0)
		{
            if(result == vMatchingHist[i].imod)
				iChecksum++;
			else
                return false;
            if(is_unmodified( vMatchingHist[i] ) )
                    return false;
		}
        else if(is_unmodified( vMatchingHist[i] ) )
			iChecksum++;

		else
            return false;
	}

    if( vMatchingHist.empty() && ! vNucleosomeHist.empty())
        return false;

    return iChecksum == vMatchingHist.size();

}
std::vector<std::vector<std::string> > parseStateOccurences(std::string& sInput)
{
    int iCheck = -1;
    std::vector<std::string> tmp1 = explode(sInput,"}");
    std::vector<std::string> tmp2;

    std::vector<std::vector<std::string> > vHistones;
    std::vector<std::string> vSites;

    unsigned int iNumNucs = tmp1.size();

    for(unsigned int i = 0; i < iNumNucs; i++)
    {
    	tmp2.clear();

    	if(tmp1[i].length() == 0 || tmp1[i].length() == 1)
       		  continue;

    	tmp1[i] = tmp1[i].substr(1,tmp1[i].length());

    	std::vector<std::string> tmp2 = explode(tmp1[i],"]");

        for(unsigned int j = 0; j < tmp2.size(); j++)
        {

            std:: string sTmpHist = tmp2[j].substr(0, tmp2[j].find("["));

            for(unsigned int k = 0; k < vHistones.size(); k++)
            {
                if(vHistones[k][0] == sTmpHist)
                {
                    iCheck = k;
                    break;
                }
                else
                    iCheck = -1;

            }

            std::string sTmpSite = tmp2[j].substr(tmp2[j].find("[")+1, tmp2[j].find(".") - tmp2[j].find("[")-1);

            int iCheck2 = 0;

            if(iCheck != -1)    //histone ex schon
            {
                for(unsigned int l = 1; l < vHistones[iCheck].size(); l++)
                {

                    if(vHistones[iCheck][l] != sTmpSite)
                    	iCheck2 = 1;
                    else
                    {
                    	iCheck2 = 0;
                      	break;
                    }
                }

                if(iCheck2 == 1)
                   	vHistones[iCheck].push_back(sTmpSite);

            }
            else
            {
                std::vector<std::string> vTmp { sTmpHist, sTmpSite };
                vHistones.push_back(vTmp);
            }
        }
    }
    return vHistones;
}


void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace)
{
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos)
    {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}
