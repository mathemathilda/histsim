
#include "nucleosome.hpp"


std::string cNucleosome::printString(void)
{
    std::string outstring = "";
    for (uint index =0 ; index < nucleosomeHist.size() ; index++)
        outstring = outstring + params::sitevector[ nucleosomeHist[index].isite ]+ "." + params::modvector[ nucleosomeHist[index].imod ]+ "]";
    return outstring;
}
/*
std::string cNucleosome::printIntString(void)
{
    std::string outstring = "";
    for (uint index =0 ; index < nucleosomeHist.size() ; index++)
        outstring.append( params::modmatrix[ nucleosomeHist[index].isite ][ nucleosomeHist[index].imod ]);
    if (outstring == "")
        outstring = "0";
    return outstring;

}
*/
