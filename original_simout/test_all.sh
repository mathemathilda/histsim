#!/bin/bash
date
mkdir completed
mkdir completed/same
mkdir completed/different
for filename in *.cfg; do
	#echo $filename
	./HistSim  --rf "$filename" --sf test-sf --pf test-pf --nondyn   -d fast   --seed 42
	./HistSim_original --rf  "$filename" --sf test-sf --pf test-pf     


	if [[ $(diff -w <(tail -n 1 simout_0/outfile.txt) <(tail -n 1 fast/0/outfile.txt)) ]]; then 
		dir=completed/different/$(basename "$filename" .cfg) 
		mkdir $dir
		mv simout_0/outfile.txt $dir/outfile.txt
	        mv fast/0/outfile.txt $dir/outfile_fast
	        echo "##############################################################  different"
	else
		dir=completed/same/$(basename "$filename" .cfg) 
		mkdir $dir
		mv simout_0/outfile.txt $dir/outfile.txt
	        mv fast/0/outfile.txt $dir/outfile_fast
		echo "##############################################################  same"
	fi
	mv $filename completed/
			       
done
# takes 000

#find . -name *.cfg | xargs --max-args=1 --max-procs=50 iteration 
date
exit 0
