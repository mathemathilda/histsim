/*
 * rule.cpp
 *
 *  Created on: May 21, 2015
 *      Author: nicoh
 */

#include "rule.hpp"
//#include <regex>
//#include <boost/regex.hpp>
//#include <boost/cregex.hpp>
#include <string>
#include <iostream>
// to treat the left right and subject strings of the target and make them a vector over hists




std::vector< std::vector<Hist>> subPattern_to_subtarget( std::string subPatternString)
{
    std::vector<std::string> tempvec;
    std::vector<std::vector<Hist>> subtarget;

    if(subPatternString != "")
        tempvec = explode(subPatternString,"}");

    Hist newhist;
    std::vector	< Hist> nucleoHist ;
    // this iterates over the relavant nucleosomes, usually one ...
    for(uint i = 0; i < tempvec.size(); i++)
    {
        if(  tempvec[i].compare(0,1,"{") == 0)
            tempvec[i] = tempvec[i].substr(1, tempvec[i].length()-1);
        std::vector < std::string >  histstring = explode (  tempvec[i], "]" );
        // this iterates over the modifications on one nucloesome, traditionnally up to two
        for ( uint j = 0; j< histstring.size(); j++)
        {
            std::vector< std::string > site_mod_histstring = explode (histstring[j], ".");
            //newhist.site = site_mod_histstring[0];
            newhist.isite = mod_to_imod( site_mod_histstring[0], params::sitevector);
            newhist.imod = mod_to_imod( site_mod_histstring[1] , params::modvector);

            nucleoHist.push_back(newhist);
        }

        subtarget.push_back(nucleoHist);
        nucleoHist.clear();
    }
    return subtarget;
}
Target string_to_target(std::string targetstring_)
{
    Target outTarget;
    std::vector<std::string> vMatch = decomposeTarget(targetstring_);
    outTarget.left = subPattern_to_subtarget( vMatch[0]);
    outTarget.subject = subPattern_to_subtarget( vMatch[1]+ "}" )[0];
    outTarget.right = subPattern_to_subtarget( vMatch[2]);
    return outTarget;
}


