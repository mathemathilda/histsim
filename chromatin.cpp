/*
 * chromatin.cpp
 *
 *  Created on: Apr 23, 2015
 *      Author: nicoh
 */

#include "chromatin.hpp"
#include "helper.hpp"
//#include <boost/lexical_cast.hpp>
#include <vector>
//#include <boost/variant/variant.hpp>
//#include <boost/lambda/lambda.hpp>
//#include <boost/lambda/bind.hpp>
//#include <boost/range/adaptor/map.hpp>
//#include <boost/range/algorithm/copy.hpp>
#include "split.hpp"

chromatin::chromatin(Simparams simparams )
{
    iNumNucleosomes = simparams.nNucleosomes;
    std::vector<cNucleosome> aNucleosomeVector;
    aNucleosomes = mInitNucleosomes(simparams);
    nucleosomestring = initNucleosomesString(simparams, aNucleosomes); // this one is buffered


}

chromatin::~chromatin()
{
	// TODO: destructor
}

std::vector <uint> chromatin::initNucleosomesString(Simparams& sp, std::vector<cNucleosome> aNucleosomes)
{
    unsigned long size = (sp.nNucleosomes + 2 * sp.buffer ) * params::sitevector.size();
    std::vector<uint> nucstring(size ,1);
    //strcpy(nucstring, std::string(size, 1).c_str() );
    //std::vector<char> nucstring;
    for (uint inuc = 0; inuc < sp.nNucleosomes  ; ++inuc) {
        for (uint isite=0; isite < params::sitevector.size(); isite++)
        {
            for (uint ihist = 0 ; ihist < aNucleosomes[inuc].nucleosomeHist.size() ; ihist++)
                if ( aNucleosomes[inuc].nucleosomeHist[ihist].isite == isite)
                {
                    nucstring[ (inuc + sp.buffer ) * params::sitevector.size() + isite]
                            = aNucleosomes[inuc].nucleosomeHist[ihist].imod ;
                }
        }
    }
    // fill the buffers
    for (uint i =0; i< sp.buffer * params::sitevector.size(); i++)
    {
        //front
        nucstring[i] =999;
        //end
        nucstring[ i + (sp.buffer + sp.nNucleosomes) * params::sitevector.size()] = 999;
    }
    // fill the buffer with a high value, so that shall not be reached by modification list
    //strcpy(buffered	, std::string(3 * sp.maxRuleReach , 127).c_str());
    return nucstring;
}
std::vector<cNucleosome> chromatin::mInitNucleosomes(Simparams& simparams)
{
    std::vector<std::string> nucleosomelevel = explode(simparams.initstate,"}");

    uint iSize = nucleosomelevel.size();

    std::vector<cNucleosome> aNucleosomeVector(iSize);

    for(uint i = 0; i < iSize; ++i)
    {
        cNucleosome tmpnuc;
        // TODO, when it runs again, try to remove iPosition, as it seems never used
        tmpnuc.iPosition = i;
        //mModifications;

        std::vector<std::string> sNucleosomeString = explode ( nucleosomelevel[i].substr(1,nucleosomelevel[i].size()-1),"]");

        std::vector<std::string> vTempMatch;

        for(uint i =0; i < sNucleosomeString.size(); i++)
        {
            vTempMatch = explode(sNucleosomeString[i],".");
            Hist temphist;

            temphist.imod  = mod_to_imod( vTempMatch[1] ,params::modvector);
            temphist.isite = mod_to_imod( vTempMatch[0] , params::sitevector);

            tmpnuc.nucleosomeHist.push_back(temphist);
        }
        aNucleosomeVector[i] = tmpnuc;
	}
	return aNucleosomeVector;
}
/*
void chromatin::mPrintStateToFile(std::ofstream* fsFilehandle)
{
	//if fsFilehandle.is_open ...
    int iNumNucleosomes = this.aNucleosomes.size();

	for(int i = 0; i < iNumNucleosomes; i++)
	{
        std::string sNucleosomeString = this.aNucleosomes[i].sNucleosomeString;
		fsFilehandle << sNucleosomeString << "\n";

	}

	//fsFilehandle <<
}*/

std::string chromatin::print_nuc(Simparams& sp, uint inuc)
{
    uint index = (inuc + sp.buffer)* params::sitevector.size();
    std::string outstring = "";
        for (uint site =0 ; site < params::sitevector.size() ; site ++)
        {
            if ( nucleosomestring [index + site ] != 1)
                outstring = outstring + params::sitevector[ site ]+ "." + params::modvector[ nucleosomestring [index + site ] ]+ "]";
        }
    return outstring;
}
/*!
 * \brief chromatin::print_short
 * \param sp
 * \return integer representation of the nucleosomestring according to the params::modmatrix
 */
std::string chromatin::print_short(Simparams& sp)
{
    std::string outstring = "";
    // nnucleosome string has 3 * sp.maxRuleReach) * params::sitevector.size() characters reserved for each nucleosome position, so the index need to be accordingly
    for (uint index = sp.buffer * params::sitevector.size() ; index < sp.nNucleosomes + sp.buffer * params::sitevector.size() ; index += params::sitevector.size() )
        for (uint site =0 ; site < params::sitevector.size() ; site ++)
        {
            outstring.append( params::modmatrix[ site ][ nucleosomestring[ index + site] ]);
        }
    return outstring;

}
std::string chromatin::print_counts(Simparams& sp)
{
    std::string outstring = "";
    // nnucleosome string has 3 * sp.maxRuleReach) * params::sitevector.size() characters reserved for each nucleosome position, so the index need to be accordingly
    std::vector<int> counts(params::modvector.size() * params::sitevector.size(),0);
    for (uint index = 0 ; index < sp.nNucleosomes ; index += params::sitevector.size() )
        for (uint site =0 ; site < params::sitevector.size() ; site ++)
            counts[ nucleosomestring[ index + sp.buffer * params::sitevector.size() + site]]++;

    for (uint mod = 1; mod < counts.size(); mod++ ){
        outstring.append(",  ");
        outstring.append(std::to_string(counts[mod]));
    }
    return outstring;
}
/*!
    Returns scalar deviation from assumed input nucleosome string in  \c correct \c wrong \c missing.
    has to be hs to be changed if the input string will be different than the default one used for developement #todo
*/
std::string chromatin::print_diff(Simparams& sp)
{
    std::string outstring = ",    ";
    // nnucleosome string has 3 * sp.maxRuleReach) * params::sitevector.size() characters reserved for each nucleosome position, so the index need to be accordingly
    int correct = 0;
    int wrong = 0;
    int missing = 0;
    for (uint index = 0 ; index < sp.nNucleosomes ; index += params::sitevector.size() )
        for (uint site =0 ; site < params::sitevector.size() ; site ++)
        {
            std::string value = params::modmatrix[ site ][ nucleosomestring[ index + sp.buffer * params::sitevector.size() + site] ];
            if ((index / 10) % 2 == 0){
                if (value == "3")
                    correct +=1;
                else if(value == "2")
                    wrong+=1;
                else
                    missing+=1;
            }
            else{
                if (value == "2")
                    correct +=1;
                else if(value == "3")
                    wrong+=1;
                else
                    missing+=1;
            }
        }
    outstring.append(std::to_string(correct));
    outstring.append(",  ");
    outstring.append(std::to_string(wrong));
    outstring.append(",  ");
    outstring.append(std::to_string( missing));

    return outstring;

}
