/*
 * Parser.cpp
 *
 *  Created on: Sep 10, 2015
 *      Author: nicoh
 */

#include "cXMLParser.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include "split.hpp"
#include "cExRule.hpp"

cParser::cParser(std::string cFilename, int iVerbosity_)
{

    iVerbosity = iVerbosity_;

    std::ifstream XMLFile(cFilename);

    //LogHandle = LogHandle_;

    //if(iVerbosity > 1)
    //	fsParseLogFilehandle = LogHandle;


    //if(iVerbosity > 2)
    //	fsDebugLogFilehandle.open("logs/parse_dbg.log");



    std::vector<char> temp((std::istreambuf_iterator<char>(XMLFile)), std::istreambuf_iterator<char>());

    buffer = temp;
    buffer.push_back('\0');

    std::string sBuff(buffer.data());

    //std::cout << sBuff;

    doc.parse<0>(&buffer[0]);

    //if(iVerbosity > 0)
    //	LogHandle << "Parsing config file... \n";

    //std::cout << "Parsing config file... \n";

    //misses rapidxml::file standard constructor, solution?
    //fileXML = rapidxml::file<>(cFilename);
    //doc.parse<0>(fileXML.data());


}

cParser::cParser(int iVerbosity_)
{
    iVerbosity = iVerbosity_;
}

void cParser::mParseConfigFile(std::string cFilename)
{
    std::ifstream XMLFile(cFilename);


    std::vector<char> temp((std::istreambuf_iterator<char>(XMLFile)), std::istreambuf_iterator<char>());

    buffer = temp;
    buffer.push_back('\0');

    std::string sBuff(buffer.data());

    try
    {
        doc.parse<0>(&buffer[0]);
    }
    catch(int e)
    {
        std::cout << "Parsing error occured: " << e << "\n";
        exit(1);
    }
    //std::cout << "Parsing config file... \n";
}

std::vector< std::vector<std::string> > cParser::mParseHistoneMap()
{
    std::map< std::string, boost::variant<std::string, int> > simparams;

    rapidxml::xml_node<> *rootnode =  doc.first_node("histoneMap");

    rapidxml::xml_node<> *node =  rootnode->first_node("histoneOrder");

    rapidxml::xml_attribute<> *attr;
    //std::cout << "\nGetting cyclic info: \n \n ";

    int iHistone;
    std::vector<std::vector<std::string> > vHistones;
    std::vector<std::string> vSites;
    
    if(node)
    {
        for(rapidxml::xml_node<> *histoneDef = node->first_node("histone"); histoneDef != 0; histoneDef = histoneDef->next_sibling())
        {
            
            vSites.clear();
            std::string sHistone =  boost::lexical_cast<std::string>(histoneDef->first_attribute("name")->value());

            //std::cout << "TEST: " << sHistone << "\n";

            vSites.push_back(sHistone);
            
            if(histoneDef->first_attribute("name")->value() != "" || histoneDef->first_attribute("name")->value())             //skipping
            {
                for (rapidxml::xml_node<> *histSite = histoneDef->first_node(); histSite; histSite = histSite->next_sibling())
                {
                    std::string sSite = histSite->first_attribute("name")->value();
                    if(sSite != "")
                        vSites.push_back(sSite);
                }
            }
            
            if(sHistone != "" && vSites.size() > 1)
                vHistones.push_back(vSites);
        }
    }
    
    return vHistones;
    
}

cParser::~cParser()
{
    fsDebugLogFilehandle.close();
    fsParseLogFilehandle.close();
}

void cParser::mCloseFile()
{
    fsDebugLogFilehandle.close();
    fsParseLogFilehandle.close();
}

std::string cParser::mParseStateFile()
{

    fsParseLogFilehandle << "\nSimparams: \n \n";


    //std::map< std::string, boost::variant<std::string, int> > simparams;

    //simparams["SetSimTime"] = 0;
    //simparams["SetIterationCount"] =0;
    rapidxml::xml_node<> *node =  doc.first_node("initialState");

    rapidxml::xml_attribute<> *attr;
    //std::cout << "\nGetting cyclic info: \n \n ";

    std::string sStartingState = "NULL";

	if(node)
	{
                //attr = node->value;
                //if(attr->value() == "")
                //{
                //        std::cout << "startingstate not defined! \n";
                //	exit(1);
                //}
                sStartingState = node->value();
                //simparams["startingstate"] = attr->value();
		if(iVerbosity > 1)
                        fsParseLogFilehandle << "\nStartingstate: " << sStartingState << " \n";

	}
        else
        {
                std::cout << "\nstartingstate not found!\n";
                exit(0);
        }


    return sStartingState;
}



Simparams cParser::mParseParamFile(std::ofstream& LogHandle)
{

    //std::cout << "\nSimparams: \n \n";


    Simparams simparams;

    simparams.SetSimTime = 0;
    simparams.SetIterationCount = 0;
    simparams.cyclic = false;
    //simparams.stopcriterion = -1;
    simparams.matrixlog = false;
    //simparams.hash = "";
    simparams.verbosity = 0;
    rapidxml::xml_node<> *node = doc.first_node("simulationParameters");
    rapidxml::xml_attribute<> *attr;

    std::vector<std::string> vTmp;
    std::vector<std::string> vTmp2;
    std::vector<std::string> vTmp3;
    std::string sTmp;
    if(node)
    {


        rapidxml::xml_node<> *oSimTimeNode = node->first_node("simulationTime");
        if(oSimTimeNode)
        {
            attr = oSimTimeNode->first_attribute("type");
            std::string sValue = attr->value();
            if(sValue == "i")
            {
                //simparams.stopcriterion = 0;
                attr = oSimTimeNode->first_attribute("value");
                simparams.SetIterationCount = atoi(attr->value());
            }
            else if(sValue == "t")
            {
                simparams.SetSimTime = atoi(attr->value());
                //simparams.stopcriterion = 1;
                attr = oSimTimeNode->first_attribute("value");
                simparams.SetSimTime = atoi(attr->value());
            }
            else
            {
                std::cout << attr->value() << ": simulation stop criterion type not defined! \n";
            }

            attr = oSimTimeNode->first_attribute("value");

        }
        else
        {
            std::cout << "simulationTime not defined in parameter file! \n";
            //simparams.stopcriterion= -1;
            simparams.SetIterationCount = -1;
            simparams.SetSimTime = -1;
        }

        rapidxml::xml_node<> *oCyclicNode = node->first_node("cyclicChromosomes");
        if(oCyclicNode)
        {
            attr = oCyclicNode->first_attribute("value");
            if(attr->value() == "")
            {
                std::cout << "Cyclic chromosomes not defined! \n";
                exit(1);
            }
            simparams.cyclic = atoi(attr->value());
        }
        else
        {
            std::cout << "Cyclic chromosomes not defined in parameter file! \n";
            //exit(1);
        }

        rapidxml::xml_node<> *oPMNode = node->first_node("propMatrices");
        if(oPMNode)
        {
            attr = oPMNode->first_attribute("value");
            if(attr->value() == "")
            {
                std::cout << "prop matrices option not defined! \n";
                exit(1);
            }
            simparams.matrixlog= atoi(attr->value());
        }
        else
        {
            std::cout << "prop matrices option not defined in parameter file! \n";
        }

        rapidxml::xml_node<> *oHashNode = node->first_node("configurationHashKey");
        if(oHashNode)
        {
            attr = oHashNode->first_attribute("value");
            if(attr->value() == "")
            {
                //std::cout << "Hash not defined! \n";
            }
            //simparams.hash = attr->value();
        }

    }
    else
    {
        std::cout << "Parameterfile ERROR! \n";
        exit(1);
    }


    return simparams;
}

std::vector<cEnzyme> cParser::mParseRuleFile(std::ofstream& LogHandle)
{

    LogHandle << "\nEnzymes: \n \n";

    //iVerbosity =2;
    std::vector<cEnzyme> vEnzymeSet;
    //rapidxml::xml_node<> *rootnode = doc.first_node("simulation");

    //rapidxml::xml_node<> *node = doc.first_node("enzymeSet");
    rapidxml::xml_node<> *node = doc.first_node("enzymeSet");
    rapidxml::xml_attribute<> *attr;

    std::vector<std::string> vTmp2;
    std::vector<std::string> vTmp3;
    std::vector<std::string> modvector;
    std::string sTmp;
    if(node)
    {
        int enzymecounter = 0;


        for(rapidxml::xml_node<> *enzymeDef = node->first_node("enzymeDef"); enzymeDef != 0; enzymeDef = enzymeDef->next_sibling())
        {
            if(strcmp(enzymeDef->name(), "enzymeDef")!=0)
                continue;

            cEnzyme newEnzyme;
            if(iVerbosity > 1)
                std::cout << "Enzyme " << enzymecounter << ":  \n";


            for (rapidxml::xml_node<> *enzymeparam = enzymeDef->first_node(); enzymeparam; enzymeparam = enzymeparam->next_sibling())
            {

                //atof strtof ???

                const char* cnodename = enzymeparam->name();
                std::string nodename(cnodename);

                if(nodename == "name")
                {
                    std::string sEnzName = enzymeparam->first_attribute("value")->value();
                    if(sEnzName.empty())	//check for non existent missing
                    {
                        std::cout << "Enzyme name not defined! \n";
                        exit(1);
                    }
                    else
                    {
                        newEnzyme.sName = enzymeparam->first_attribute("value")->value();
                    }

                    /*std::string sEnzEnab = enzymeparam->first_attribute("enabled")->value();
                    if(sEnzEnab.empty())
                    {
                        std::cout << "Enzyme enable status not defined! \n";
                        exit(1);
                    }
                    else
                    {
                        newEnzyme.iIsActive = atoi(enzymeparam->first_attribute("enabled")->value());
                    }
                    */
                    newEnzyme.iIsActive = true;	//remove completely later on

                    if(iVerbosity > 1)
                        std::cout << "\tName: " << newEnzyme.sName << " \n";

                }
                else if(nodename == "size")
                {
                    std::string sEnzSize = enzymeparam->first_attribute("value")->value();
                    if(sEnzSize.empty())
                    {
                        std::cout << "Enzyme size not defined! \n";
                        exit(1);
                    }
                    else
                    {
                        newEnzyme.iBindingSize = floor(atoi(enzymeparam->first_attribute("value")->value())/2);
                    }
                    if(iVerbosity > 1)
                        std::cout << "\tSize: " << newEnzyme.iBindingSize << " \n";
                }
                else if(nodename == "concentration")
                {
                    std::string sEnzConc = enzymeparam->first_attribute("value")->value();
                    if(sEnzConc.empty())
                    {
                        std::cout << "Enzyme concentration not defined! \n";
                        exit(1);
                    }
                    else
                    {
                        float fEnzConc = atof(enzymeparam->first_attribute("value")->value());
                        newEnzyme.conc = fEnzConc; //TODO: type unhandled
                    }
                    if(iVerbosity > 1)
                        std::cout << "\tConcentration: " << newEnzyme.conc << " \n";
                }
                else if(nodename == "bindingRatesRuleSet")
                {



                    if(iVerbosity > 1)
                        std::cout << "\tRule: \n";



                    Rule pNewRule;


                    for (rapidxml::xml_node<> *ruleparam = enzymeparam->first_node(); ruleparam; ruleparam = ruleparam->next_sibling())
                    {
                        const char* crulenodename = ruleparam->name();
                        std::string rulenodename(crulenodename);

                        if(rulenodename == "rule")
                        {
                            if(ruleparam->first_attribute("value")->value() == "")
                            {
                                std::cout << "Rule rewrite string not defined! \n";
                                exit(1);
                            }
                            std::vector< std::string> ttmpwrite = decomposeTarget(ruleparam->first_attribute("value")->value());
                            std::vector< std::string > tmpwrite = explode(ttmpwrite[1] , "]");
                            Hist newhist;
                            for ( int index =0; index < tmpwrite.size(); index++)
                            {
                                newhist.isite = mod_to_imod(  explode (tmpwrite[index], ".")[0] , params::sitevector );
                                newhist.imod = mod_to_imod(  explode (tmpwrite[index], ".")[1] , params::modvector );
                                pNewRule.write.push_back(newhist);
                            }

                            //if(iVerbosity > 1)
                            //std::cout << "\t\tRewrite to: " << pNewRule.write[] << " \n";
                            // todo write out array of write mask

                        }
                        else if(rulenodename == "name")
                        {
                            std::string sRuleName = ruleparam->first_attribute("value")->value();
                            if(sRuleName.empty())
                            {
                                std::cout << "Rule name not defined! \n";
                                exit(1);
                            }
                            else
                            {
                                pNewRule.name = ruleparam->first_attribute("value")->value();
                            }
                            if(iVerbosity > 1)
                                std::cout << "\t\tName: " << pNewRule.name << " \n";
                        }
                        else if(rulenodename == "target")
                        {
                            if(ruleparam->first_attribute("value")->value() == "")
                            {
                                std::cout << "Rule matching target not defined! \n";
                                exit(1);
                            }
                            pNewRule.target = string_to_target( ruleparam->first_attribute("value")->value() );
                        }
                        else if(rulenodename == "rate")
                        {

                            if(ruleparam->first_attribute("value")->value() == "")
                            {
                                std::cout << "Rule rate not defined! \n";
                                exit(1);
                            }
                            pNewRule.rate = boost::lexical_cast<float>(ruleparam->first_attribute("value")->value());

                            if(iVerbosity > 1)
                                std::cout << "\t\tRate: " << pNewRule.rate << " \n";
                        }
                        else if(rulenodename == "enabled")
                        {
                            if(ruleparam->first_attribute("value")->value() == "")
                            {
                                std::cout << "Rule enabling not defined! \n";
                                exit(1);
                            }

                            pNewRule.enabled = boost::lexical_cast<bool>(ruleparam->first_attribute("value")->value());
                            if(iVerbosity > 1)
                                std::cout << "\t\tEnabled: " << pNewRule.enabled << " \n";
                        }
                        else if(rulenodename == "dissociationRate")
                        {
                            if(ruleparam->first_attribute("value")->value() == "")
                            {
                                std::cout << "Rule dissociation rate not defined! \n";
                                exit(1);
                            }
                            //newEnzyme.fDissociationRate = boost::lexical_cast<float>(enzymeparam->first_attribute("value")->value()); //TODO: type unhandled
                            pNewRule.dissociationRate = boost::lexical_cast<float>(ruleparam->first_attribute("value")->value());
                            //std::cout << "dissocrate: " << pNewRule.fDissociationRate << "\n";
                            if(iVerbosity > 1)
                                std::cout << "\t\tDissociation rate: " << pNewRule.dissociationRate << " \n";
                        }
                    }

                    if(pNewRule.enabled)
                    {
                        newEnzyme.mAddRule(pNewRule);
                        //if(iVerbosity > 2)
                        //fsDebugLogFilehandle << newEnzyme.sName << ";" << newEnzyme.fConcentration << ";" << pNewRule.targetString << "->" << pNewRule.write[0] << ";" << pNewRule.rate << ";" << pNewRule.fDissociationRate << "\n";
                        // todo write out array of write mask

                    }
                }
            }
            vEnzymeSet.push_back(newEnzyme);

            enzymecounter++;
        }

    }
    else
    {
        std::cout << "Enzyme set not defined! \n";
        exit(1);
    }

    //std::cout << "Rules parsed \n";

    return vEnzymeSet;
}


std::vector<std::string> cParser::mParseMatchingPattern(std::string sNuc)
{
    std::vector<std::string> vHistoneVector;

    std::vector<std::string> vLeftSide;
    std::vector<std::string> vRightSide;

    std::vector<std::string> vTemp;
    std::vector<std::string> vOut;

    boost::split(vLeftSide, sNuc, boost::is_any_of("("));

    boost::split(vRightSide, vLeftSide[1], boost::is_any_of(")"));

    //std::cout << sNuc << " splitted into ";
    //std::cout << vLeftSide[0] << " and ";		//left
    //std::cout << vRightSide[0] << " and ";		//middle (target)
    //std::cout << vRightSide[1] << "\n";			//right



    //decompose left side into single nucleosomes

    boost::split(vTemp, vLeftSide[0], boost::is_any_of("}"));

    //std::cout << "Left side nucleosomes: \n";

    if(vLeftSide[0] == "")
    {
        //std::cout << "No left side!\n";
        vOut.push_back("x");
    }
    else
    {
        for(unsigned int i = 0; i < vTemp.size()-1; i++)
        {
            vTemp[i] = vTemp[i].substr(1, vTemp[i].size() - 1);


            vOut.push_back(vTemp[i]);

            //std::cout << i << ": " << vTemp[i] << "\n";
        }
    }

    vOut.push_back("-");



    //std::cout << "Matching target nucleosome: \n";

    if(vRightSide[0] == "")
    {
        //std::cout << "Error! No matching Target!\n";
        exit(1);
    }
    else
    {
        vRightSide[0] = vRightSide[0].substr(1, vRightSide[0].size() - 2);



        vOut.push_back(vRightSide[0]);

        //std::cout << vRightSide[0] << "\n";

    }

    vOut.push_back("-");

    boost::split(vTemp, vRightSide[1], boost::is_any_of("}"));

    //std::cout << "Right side nucleosomes: \n";

    if(vRightSide[1] == "")
    {
        //std::cout << "No right side!\n";
        vOut.push_back("x");
    }
    else
    {
        for(unsigned int i = 0; i < vTemp.size()-1; i++)
        {
            vTemp[i] = vTemp[i].substr(1, vTemp[i].size() - 1);


            vOut.push_back(vTemp[i]);

            //std::cout << i << ": " << vTemp[i] << "\n";
        }
    }


    return vOut;
}



std::map< int, std::vector<std::string> > cParser::mParseNucleosome(std::vector<std::string> vNuc)
{
    std::map< int, std::vector<std::string> > mHistoneMap;


    for(unsigned int i = 0; i < vNuc.size(); i++)
    {

        if(vNuc[i] == "x")
        {


        }
        std::vector<std::string> vTemp;

        boost::split(vTemp, vNuc[i], boost::is_any_of("]"));

        for(unsigned int j; j< vTemp.size(); j++)
        {
            //mHistoneMap[i][j]

        }

    }

    return mHistoneMap;
}

int cParser::checkSyntax(std::string sSubject, int iMode)
{
    int curly_brackets = 0;
    int parenthesis = 0;
    int square_brackets = 0;
    int dot = 0;

    for (uint i = 0;i < sSubject.size(); i++)
    {
        //echo $i." - ".$teststring[$i]."<br>";

        if(sSubject[i] == '{')
        {
            if(square_brackets != 0)
            {
                std::cout << "Syntax Error! Closing ] missing!";
                return 0;
            }
            curly_brackets++;
        }
        if(sSubject[i] == '}')
        {
            curly_brackets--;
            if(square_brackets != 0)
            {
                std::cout << "Syntax Error! Closing ] missing!";
                return 0;
            }
        }
        if(sSubject[i] == '(')
            parenthesis++;
        if(sSubject[i] == ')')
            parenthesis--;

        if(sSubject[i] == '[')
        {
            if(curly_brackets != 1)
            {
                std::cout << "Syntax Error! [ outside of nucleosome or opening { missing!";
                return 0;
            }
            square_brackets++;
        }
        if(sSubject[i] == ']')
        {
            if(curly_brackets != 1)
            {
                std::cout << "Syntax Error! ] outside of nucleosome or opening { missing!";
                return 0;
            }
            dot = 0;
            square_brackets--;
        }
        if(sSubject[i] == '.' && curly_brackets != 1 && square_brackets != 1)
        {
            std::cout << "Syntax Error! . outside of histone definition or opening [ missing!";
            return 0;
        }
        if(sSubject[i] == '.')
        {
            dot++;
        }
        if(dot > 1)
        {
            std::cout << "Syntax Error! Multiple . in histone definition!";
            return 0;
        }
    }

    if(curly_brackets != 0)
    {
        std::cout << "Syntax Error! Missing opening or closing curly bracket!";
        return 0;
    }
    if(parenthesis != 0)
    {
        std::cout << "Syntax Error! Missing opening or closing parenthesis!";
        return 0;
    }
    if(square_brackets != 0)
    {
        std::cout << "Syntax Error! Missing opening or closing square bracket!";
        return 0;
    }


    return 1;
}
