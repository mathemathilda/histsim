/*
 * cExRule.cpp
 *
 *  Created on: Mar 29, 2017
 *      Author: nicoh
 */

#include "cExRule.hpp"

cExRule::cExRule(int iStart_, int iEnd_, std::string sName_, std::string sCheckMod_, std::string sCheckType_, float fCheckAmount_, std::string sEffectType_, float fEffectValue_, float fEffectDelay_)
{
	iRegionStart = iStart_;
	iRegionEnd = iEnd_;
	sRegionName = sName_;
	sCheckMod = sCheckMod_;
	sCheckType = sCheckType_;
	fCheckAmount = fCheckAmount_;
	sEffectType = sEffectType_;
	fEffectValue = fEffectValue_;
	fEffectDelay = fEffectDelay_;
}


cExRule::cExRule()
{

}

cExRule::~cExRule() {
	// TODO Auto-generated destructor stub
}

