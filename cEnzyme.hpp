/*
 * cEnzyme.hpp
 *
 *  Created on: May 21, 2015
 *      Author: nicoh
 */

#ifndef CENZYME_HPP_
#define CENZYME_HPP_

#include "rule.hpp"
#include <vector>
class cEnzyme
{
public:
	//cEnzyme(std::string sName_, int iBindingSize_, float fConcentration_, float fDissociationRate_);
	cEnzyme();
	virtual ~cEnzyme();


	std::string sName;
    uint iBindingSize;
    float conc;  // concentration
    bool iIsActive;

    //uint iNumRules;
    std::vector<Rule> rules; //pointers of rules
    void mAddRule(Rule rule_);
};
struct Enzymeset
{
    std::vector<cEnzyme> enz ;
};


#endif /* CENZYME_HPP_ */
