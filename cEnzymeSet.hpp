/*
 * cEnzymeSet.hpp
 *
 *  Created on: May 21, 2015
 *      Author: nicoh
 */

#ifndef CENZYMESET_HPP_
#define CENZYMESET_HPP_

#include "cEnzyme.hpp"
#include <vector>

class cEnzymeSet
{
public:
	cEnzymeSet();
	virtual ~cEnzymeSet();

	void mAddEnzyme(cEnzyme* pEnzyme);   //void?
	void mPrintInfo();
//private:
	int iNumEnzymes;
	std::vector<cEnzyme*> aEnzymes; //pointers of enzyme objects
};



#endif /* CENZYMESET_HPP_ */
