/*
 * cEnzymeSet.cpp
 *
 *  Created on: May 21, 2015
 *      Author: nicoh
 */

#include "cEnzymeSet.hpp"

cEnzymeSet::cEnzymeSet()
{
	iNumEnzymes = 0;
}

cEnzymeSet::~cEnzymeSet()
{
}


void cEnzymeSet::mPrintInfo()
{
}

void cEnzymeSet::mAddEnzyme(cEnzyme* pEnzyme)
{
	aEnzymes.push_back(pEnzyme);
	iNumEnzymes++;
}
