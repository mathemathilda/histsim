/*
 * simparams.hpp
 *
 *  Created on: august 4th 2018
 *      Author: Tilda
 */

#ifndef SIMPARAMS_HPP_
#define SIPARAMS_HPP_

#include <string>
#include <vector>
#include "cEnzyme.hpp"
//struct or class?
#include <fstream>

// all parameterss that are constatnt during simulation
struct Simparams
{
    float SetSimTime;
    uint SetIterationCount;
    bool cyclic;
    std::string initstate;
   // int stopcriterion;
    bool matrixlog;
    int outputmode;
    uint verbosity;
    bool verbose;
    std::string outputDirectory;
    //std::string hash;
    uint nNucleosomes;
    float tdr;
    uint numRules;
    std::vector<cEnzyme> enzymes;
    uint numEnzymes;
    uint iNumChannelGroups;
    std::vector<uint> c2e; // index conversion counter to enzyme
    std::vector<uint> c2r; // index conversion counter to rule
    std::vector<std::string> vChannelTransl;
    bool nonDynamiConcentrations;
    uint nRuns;
    std::ofstream* fsMatrixFilehandle;
    std::ofstream* fsSimLogFilehandle;
    std::ofstream* fsAlgoLogFilehandle;
    std::ofstream* fsReactionLogFilehandle;
    std::ofstream* outfile_short;
    uint maxRuleReach;
    uint buffer;// buffer the nucleosome string so that we can check upon de/attachment of enzyme, what other
                // binding sites are affected without having to considder boarders of array
    std::vector< bool> zerovec; //
    std::vector<float> writeTimeSteps;
    uint writeSteps;
};

// non constant variables during simulation
class  Simvars
{
public:
    //std::vector<std::vector< float > > mReactionRates;
    //std::vector<std::vector< bool > > b_mReactionRates;
    std::vector<std::vector< bool > > bmReactionRates; // reaction rates are constant any way assuming that the concentration is const
    std::vector<std::vector< bool > > ratesReaction;
    uint iReactionIndexChannel ;
    uint iReactionIndexNucleosome ;
    std::vector <float> channelRates;
    std::vector <uint> channelSiteCount;
    std::vector <uint> enzConcentr;
    std::vector <uint> c_hannelSiteCount;
    std::vector<float> vRowSums;
    std::vector<float> vColSums;
    std::vector<int> boundEnzymes;// over nucleosomes

    float propSum;
    float prePropSum;
    float propRunner;
    float propTarget;
    float reactionProbability;
    uint seed;
    bool outputIteration;
    uint writeprogress;
    void init(Simparams sp)
    {
        for ( uint i = 0 ; i < sp.enzymes.size(); i++)
            enzConcentr.push_back(sp.enzymes[i].conc);
    }
};

#endif /* SIMPARAMS_HPP_ */
