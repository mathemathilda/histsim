/*
 * Parser.hpp
 *
 *  Created on: Sep 10, 2015
 *      Author: nicoh
 */

#ifndef PARSER_HPP_
#define PARSER_HPP_

#include "cEnzyme.hpp"
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>
#include <boost/lexical_cast.hpp>
#include "cExRule.hpp"
#include "chromatin.hpp"
#include "split.hpp"

//#include "cEnzymeSet.hpp"
//#include <boost/algorithm/string/split.hpp>
//#include <boost/algorithm/string.hpp>

class cParser
{
private:
    rapidxml::xml_document<> doc;
    std::ofstream fsParseLogFilehandle;
    std::ofstream fsDebugLogFilehandle;
    std::vector<char> buffer;
    int iVerbosity;
public:

    cParser(std::string cFilename, int iVerbosity_);
    cParser(int iVerbosity_);

    virtual ~cParser();

    void mParseConfigFile(std::string cFilename);
    std::vector< std::vector<std::string> > mParseHistoneMap();

    int checkSyntax(std::string sSubject, int iMode);
    std::map< int, std::vector<std::string> > mParseNucleosome(std::vector<std::string> vNuc);
    std::vector<std::string> mParseMatchingPattern(std::string sNuc);
    Simparams mParseParamFile(std::ofstream& LogHandle);
    std::string mParseStateFile();
    std::vector<cEnzyme> mParseRuleFile(std::ofstream& LogHandle);
    //std::vector<cExRule> mParseExRules(std::ofstream& LogHandle);
    void mCloseFile();

    //std::vector<> mParseOrigins();
    //int mGetNumRules();
};

#endif /* PARSER_HPP_ */
