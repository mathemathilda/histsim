#!/bin/bash
N=1000
date
mkdir completed
for filename in *.cfg; do
	((i=i%N)) ; ((i++==0)) && wait
	#echo $filename
	./HistSim  --rf "$filename" --sf test-sf --pf test-pf --nondyn -o 0 -r 50 -d completed/"$(basename "$filename" .cfg)" & 
done

date
exit 0
