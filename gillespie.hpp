/*
 * gillespie.hpp
 *
 *  Created on: 21.05.2015
 *      Author: nicoh
 */

#ifndef GILLESPIE_HPP_
#define GILLESPIE_HPP_

#include "chromatin.hpp"
#include <string>
#include "cEnzyme.hpp"
#include "rule.hpp"
//#include <map>
//#include "cExRule.hpp"


chromatin runGillespie_exp(chromatin startingstate, Simparams simparams, Simvars &sv, std::string outputdir); //ruleset rules, simparams params
void findreaction(Simparams &sp, Simvars &sv );
void initReactionStatus(chromatin &state, Simparams& simparams, Simvars& sv);
void updateReactionsInRange(chromatin& state, Simparams& sp, Simvars& sv,uint min, uint max);
void updatePossibleReactions( chromatin &state, Simparams& simparams, Simvars& sv);
void initPossibleReactions( chromatin &state, Simparams& simparams, Simvars& sv);
void updateReactionStatusFast( chromatin &state, Simparams& simparams, Simvars& sv);

//<<<<<<< HEAD
void getRateForPositionCompare(const uint& irule, const uint &pos, Rule &rule, chromatin &state, const uint &iEnzymeIndex, Simparams &sp, Simvars& sv);
void updatePosition(Simvars& sv, const uint &irule, const uint &pos);
//=======
//void initReactionStatus(int &iNumChannelGroups, std::vector<uint> &vEnzymeTransl, std::vector<int> &vRuleTransl, std::vector<int> &vBoundEnzymes, chromatin &state, std::vector<float> &vRowSums, std::vector<float> &vColSums, std::vector<std::vector<float> > &mReactionRates, std::vector<int> &vEnzymeConc, std::ofstream& LogHandle, Simparams& simparams);
//bool checkside(const int& pos, Rule &rule, chromatin& state, std::vector<std::vector<Hist> > &histones,  const  Simparams& simparams , int side);
//>>>>>>>  repaired the broken folder creation

void updatePropensitySum(Simparams& sp, Simvars& sv);

void applyRule(chromatin &state, const Rule &rule, Simvars &sv, Simparams& sp);

void printVectorToFile(std::vector<std::string>, std::ofstream& Handle);
void printVectorToFile(std::vector<int>, std::ofstream& Handle);
void printVectorToFile(std::vector<float>, std::ofstream& Handle);

std::string writeCSV(std::vector<std::vector<float> > matrix, std::vector<std::string> rowDesc, std::vector<std::string> colDesc);

#endif /* GILLESPIE_HPP_ */
