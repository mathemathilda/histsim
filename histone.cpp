

#include "histone.hpp"
#include"params.hpp"


bool is_unmodified(Hist& hist)
{
    if (hist.imod == 1)
        return true;
    return false;
}

uint  getHistValue(std::vector<Hist>& nucleosome, uint isite)
{
    for(uint i = 0; i < nucleosome.size(); i++)
	{
        if(nucleosome[i].isite == isite)
            return nucleosome[i].imod;
	}
    return -1;
}

uint mod_to_imod (std::string& mod, std::vector<std::string>& translationTable)
// gives back the position of a modification i the table
{
   for (uint imod =0; imod < translationTable.size(); imod++)
   {
       if (mod == translationTable[imod])
           return imod;
   }
    // in case the modification is not in the list yet...
   translationTable.push_back(mod);
   return translationTable.size()-1;
}
