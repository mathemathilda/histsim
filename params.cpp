#include "params.hpp"
namespace params
{
     std::vector<std::string> modvector; // this stores modifications like "*" aka wildcard, "un", "ac" or "me"
     std::vector<std::string> sitevector; // this stores sites like "H3K4"
     std::vector<std::vector< std::string>> modmatrix;
};

