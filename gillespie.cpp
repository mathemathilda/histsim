
#include "gillespie.hpp"
#include <random>
#include <fstream>
#include "HistSim.hpp"

chromatin runGillespie_exp(chromatin chromatin, Simparams sp, Simvars& sv, std::string outputdir)
{

    uint64 ui64Time = GetTimeMs64();

    std::ofstream fsSimLogFilehandle;
    if(sp.verbose)
    {
        (fsSimLogFilehandle).open(outputdir + "logs/sim.log",std::ios_base::app );  //append as file was already used in main func

        if(sp.verbosity > 2)
        {
            (fsSimLogFilehandle).open(outputdir + "logs/reaction.log");
            (*sp.fsAlgoLogFilehandle).open(outputdir + "logs/algo.log");
        }
        (fsSimLogFilehandle) << "Initializing simulation.\n";
    }


    // initialize bound enzymes vector to empty/unbound here -1
    for(uint j = sp.buffer; j < sp.nNucleosomes + sp.buffer; j++)
        sv.boundEnzymes[j] = -1;

    //initialize marsenne twister random number generator
    std::mt19937 mt(sv.seed );
    std::uniform_real_distribution<float> dist(0, 1); // seldomly reaches boarders 0,1 so we check later

    float fRand1 = 0;
    float fRand2 = 0;

    float f_tau = 0;
    float fCurTime = 0;
    uint iIteration = 0;
    //sv.iReactionIndexChannel = 0;
    //sv.iReactionIndexNucleosome = 0;
    uint iEnzymeIndex = 0;
    uint iRuleIndex = 0;

    float fNextTime =0;

    std::ofstream outfile_long;
    std::ofstream outfile_partial;
    std::ofstream outfile_short;

    outfile_short.open(outputdir + "/outfile_short.txt" ,std::ios_base::app);   //, ios::app);

    if (sp.outputmode > 0){
        if(sp.outputmode == 1 )
            outfile_long.open(outputdir + "outfile.txt");   //, ios::app);
        else if(sp.outputmode == 2 )
            outfile_partial.open(outputdir + "outfile_reduced.txt");   //, ios::app);
        //write simulation hash key for repeatibility of the results
       // outfile_long << "#" << sp.hash << "#" << "\n";
    }
    if(sp.cyclic){
        // copy the front to the end buffer and the end to the front buffer
        uint strlength = sp.nNucleosomes * params::sitevector.size();
        uint buflength = sp.buffer * params::sitevector.size();
        uint reachlength = sp.maxRuleReach * params::sitevector.size();
        for (uint pos = buflength  ; pos < buflength + reachlength ; pos++   ){
            chromatin.nucleosomestring[ strlength  + pos ] = chromatin.nucleosomestring[ pos ];
        }
        for (uint pos = buflength -  reachlength ; pos < buflength ; pos++   ){
            chromatin.nucleosomestring[  pos ] = chromatin.nucleosomestring[strlength +  pos ];
        }
    }


    // init partial propensity sums
    initPossibleReactions(chromatin, sp, sv);
    // sum up partial propensity sums
    updatePropensitySum(sp, sv);

    if(sp.verbose)
        (fsSimLogFilehandle) << "Starting Simulation\n";

    float fEndValueTarget = 0;
    if(sp.SetIterationCount > 0)
        fEndValueTarget = static_cast<float>(sp.SetIterationCount);
    else if(sp.SetSimTime > 0)
        fEndValueTarget = sp.SetSimTime;
    float fEndParam = 0;

    /////////////////////////////
    while(fEndParam < fEndValueTarget)
    {

        fRand1 = dist(mt);
        fRand2 = dist(mt);
        if (fRand1 <= 0 || fRand1 >= 1 || fRand2 <= 0 || fRand2 >= 1){
            std::cout << " bad random numbers ...";
            continue; // let's just choose new ones
        }

        //********************************************
        //abort if no reactions possible
        //********************************************

        if(sv.propSum <= 0)
        {
            if(sp.verbose)
                std::cout << "No more Possible Reactions, propensity sum is 0. Aborting!\n";
            break;
        }

        sv.propTarget = fRand2 * sv.propSum;

        if(sp.SetIterationCount > 0)
            fEndParam = iIteration;
        else if(sp.SetSimTime > 0)
            fEndParam = fCurTime;
        // determine whether recursion is of a time that shall be printed:
        // nonzero outputmode overwrites outputiteration to be always true
        if (sp.writeSteps > 0 && iIteration % sp.writeSteps == 0)
            sv.outputIteration = true;
        else if (sp.outputmode  == 1 || sp.outputmode == 3)
            sv.outputIteration = true;
        else
            sv.outputIteration = false;
        if (sp.writeTimeSteps.size() > sv.writeprogress){
            if (fCurTime > sp.writeTimeSteps[sv.writeprogress] ) {
                sv.outputIteration = true;
                sv.writeprogress++;
            }
            else
                sv.outputIteration = false;
        } else if ( sp.writeTimeSteps.size() > 0 ){
            // turn off again after last write step has been reached
            sv.outputIteration = false;
        }

        if(sp.verbose)
        {
        //****************************
        //print status info to log
        //****************************
            (fsSimLogFilehandle) << "Iteration " << iIteration << " (Current time: " << fCurTime << ")\n \n";
            (fsSimLogFilehandle) << "\n Reaction matrix:\n \n";
            (fsSimLogFilehandle) << "\n Bound enzymes:\n \n";
            printVectorToFile(sv.boundEnzymes, (fsSimLogFilehandle));
            (fsSimLogFilehandle) << "\n Enzyme concentration:\n \n";
            (fsSimLogFilehandle) << "\n Row sums:\n \n";
            printVectorToFile(sv.vRowSums, (fsSimLogFilehandle));
            (fsSimLogFilehandle) << "\n Column sums:\n \n";
            printVectorToFile(sv.vColSums, (fsSimLogFilehandle));
        }
        if(sp.matrixlog)
        {
        //prop matrix logging
            std::string sMatrixfile = outputdir + "prop_matrices/"+std::to_string(iIteration)+".csv";
            (*sp.fsMatrixFilehandle).open(sMatrixfile.c_str());
        }

        //********************************
        // choose reaction
        //********************************

        findreaction( sp , sv );
        if(sp.verbose)
        {
            (fsSimLogFilehandle) << "Rand1: " << fRand1 << "\nRand2: " << fRand2 << "\n";

            if(sp.verbosity > 2)
                (*sp.fsAlgoLogFilehandle) << iIteration << "\t" << fRand1 << "\t" << fRand2 << "\n";
            (fsSimLogFilehandle) << "\nChoose reaction: \n \tprop sum: " << sv.propSum << "\n \tprop sum to reach: " << sv.propTarget<< "\n";
            (fsSimLogFilehandle) << "\nFound random channel: " << sv.iReactionIndexChannel << ", " << sv.iReactionIndexNucleosome << "\n";
            (fsSimLogFilehandle) << "Reaction at nucleosome " << sv.iReactionIndexNucleosome << "\n";
        }

        if(sp.matrixlog)
            (*sp.fsMatrixFilehandle) << sv.iReactionIndexNucleosome << "|" << sv.iReactionIndexChannel  << "\n\n";

        //get enzyme and rule index
        iEnzymeIndex = sp.c2e[sv.iReactionIndexChannel];
        iRuleIndex = sp.c2r[sv.iReactionIndexChannel];


        //*************************
        //apply chosen reaction
        //*************************
        uint bindingsize =  sp.enzymes[iEnzymeIndex].iBindingSize;
        uint assocIdx = 0;
        if(sv.iReactionIndexChannel % 2 == 1)	//dissociation reaction
        {
            assocIdx = sv.iReactionIndexChannel -1;

            //change nucleosome state
            applyRule(chromatin, sp.enzymes[iEnzymeIndex].rules[iRuleIndex], sv, sp);

            //free enzymes and update concentration
            sv.enzConcentr[iEnzymeIndex]++;
            sv.prePropSum -= sp.enzymes[sp.c2e[assocIdx]].rules[sp.c2r[assocIdx]].dissociationRate;
            if ( ! sp.nonDynamiConcentrations)
                sv.channelRates[assocIdx] = sp.enzymes[sp.c2e[assocIdx]].rules[sp.c2r[assocIdx]].rate * sv.enzConcentr[sp.c2e[assocIdx]] ;

            // free positions covered by dissocotiating enzyme
            for(uint i = sv.iReactionIndexNucleosome - bindingsize + sp.buffer; i < sv.iReactionIndexNucleosome + bindingsize + 1 + sp.buffer; i++)
                sv.boundEnzymes[i] = -1;

            //update dissociation channel

            sv.bmReactionRates[sv.iReactionIndexChannel][sv.iReactionIndexNucleosome + sp.buffer] = 0;
            if ( sv.channelSiteCount[sv.iReactionIndexChannel] < 1 )
                sv.channelSiteCount[sv.iReactionIndexChannel]++; // you should never get here
            sv.channelSiteCount[sv.iReactionIndexChannel]--;

        }
        else										//binding reaction
        {
            //bind enzymes and update concentration
            sv.enzConcentr[iEnzymeIndex]--;
            assocIdx = sv.iReactionIndexChannel ;
            sv.prePropSum += sp.enzymes[sp.c2e[assocIdx]].rules[sp.c2r[assocIdx]].dissociationRate;
            if ( !sp.nonDynamiConcentrations)
                sv.channelRates[assocIdx] = sp.enzymes[sp.c2e[assocIdx]].rules[sp.c2r[assocIdx]].rate * sv.enzConcentr[sp.c2e[assocIdx]] ;


            for(uint i = sv.iReactionIndexNucleosome - bindingsize + sp.buffer; i < sv.iReactionIndexNucleosome + bindingsize + 1 + 3 * sp.maxRuleReach; i++)
                sv.boundEnzymes[i] = sv.iReactionIndexChannel;	//NEW

            //update dissociation channel
            sv.bmReactionRates[sv.iReactionIndexChannel+1][sv.iReactionIndexNucleosome+ sp.buffer] = 1;
            sv.channelSiteCount[sv.iReactionIndexChannel+1]++;

            if (sp.verbose)
                (fsSimLogFilehandle) << "Enzyme binds from Position " << sv.iReactionIndexNucleosome - bindingsize << " to " << sv.iReactionIndexNucleosome + bindingsize << "\n";

        }

        //********************************************
        //update propensity matrix and propensity sum
        //********************************************

        if(sp.verbose)
            (fsSimLogFilehandle) << "Updating ReactionStatus: \n";

        updatePossibleReactions( chromatin, sp, sv);
        updatePropensitySum( sp, sv);

        if(sp.verbose)
            (fsSimLogFilehandle) << "Updating Propsums: " << sv.propSum << "\n";

        //********************************************
        //print outfile information
        //********************************************


        if (sv.outputIteration ){
            // write short output only if this loop was invoked for --write option being used.
            if ( sp.writeSteps > 0  ){
                outfile_short << chromatin.print_short(sp)  ;
                outfile_short << chromatin.print_counts(sp);
                char iterationAndTimeString[40];
                std::sprintf(iterationAndTimeString, ",\t%i,\t%i,\t%.5e,\n", sp.nRuns,  iIteration , fCurTime );
                outfile_short << iterationAndTimeString;
            }
            else if(sp.outputmode == 1)
            {
                char outtext[40];
                sv.reactionProbability = sv.channelRates[sv.iReactionIndexChannel] / sv.propSum;
                std::sprintf(outtext, "\n>%i %.5e %i %i %.7f\n",  iIteration , fCurTime ,  sv.iReactionIndexNucleosome , sv.iReactionIndexChannel, sv.reactionProbability );
                outfile_long << outtext;
                //   write state to outfile
                for(uint i = 0; i < sp.nNucleosomes; i++)
                    outfile_long << "{" << chromatin.print_nuc(sp, i ) << "}:" << sv.boundEnzymes[i + sp.buffer] << ";" ;
            }
            else if(sp.outputmode == 2 && fCurTime >= fNextTime)
            {
                    outfile_partial << "\n>" << iIteration << " " << fCurTime << " " <<  sv.iReactionIndexNucleosome << " " << sv.iReactionIndexChannel << " " << fNextTime << "\n";

                    for(uint i = 0; i < sp.nNucleosomes; i++)
                        outfile_partial << "{" << chromatin.print_nuc(sp, i ) << "}:" << sv.boundEnzymes[i] << ";";	//NEW: changed

                    fNextTime = fCurTime + sp.tdr;
            }
        }

        //********************************************
        //calculate next timestep
        //********************************************


        f_tau = (1/sv.propSum) * logl(1/fRand1);
        fCurTime += f_tau;
        iIteration++;

        if(sp.verbose)
            (fsSimLogFilehandle) << " tau: " << f_tau << "\n \n \n";
        if (sp.matrixlog)
            (*sp.fsMatrixFilehandle).close();

    }  //end simulation while
    outfile_long.close();
    outfile_partial.close();
    if(sp.verbosity > 2)
        (*sp.fsAlgoLogFilehandle).close();

    if (sp.nRuns < 5)
    {
        std::cout << "Simulation finished!\n";
        ui64Time = GetTimeMs64() - ui64Time;
        std::cout << "bare simulation time: " << ui64Time << " ms\n";
        std::cout << "Outfile written to " << outputdir << "outfile.txt\n";
    }

    if ( sp.writeSteps == 0  ){
        //std::ofstream outfile_short;
        //outfile_short.open(outputdir + "/outfile_short.txt", std::ofstream::app);
        outfile_short << chromatin.print_short(sp)  ;
        outfile_short << chromatin.print_counts(sp);
        char iterationAndTimeString[40];
        std::sprintf(iterationAndTimeString, ",\t%i,\t%i,\t%.5e,\n", sp.nRuns,  iIteration , fCurTime );
        outfile_short << iterationAndTimeString;
        outfile_short.close();
    }

    return chromatin;
}

// find according reaction given the propensity sum the propensity target, ie a rand number in the prop sum
void findreaction(Simparams& sp, Simvars& sv ){
    sv.propRunner = 0;
    // first find channel as the reaction sites per channel are known
    for(uint i = 0; i < sp.iNumChannelGroups; i++)
    {
        // leap foward if the channel does not reach the target
        if (sv.propRunner + sv.channelRates[i] * sv.channelSiteCount[i] < sv.propTarget)
        {
            sv.propRunner += sv.channelRates[i] * sv.channelSiteCount[i];
            continue;
        } // else:
        // find specific reaction that happens, ie the nucleosome on which the reacton happens
        for(uint j = 0; j < sp.nNucleosomes; j++)
            if (sv.bmReactionRates[i][j+ sp.buffer]) //ie if the reaction is marked possible
            {
                sv.propRunner += sv.channelRates[i];
                // when the runner is past the random target it is in the right bin
                if (sv.propRunner >= sv.propTarget)
                {
                    sv.iReactionIndexChannel    = i;
                    sv.iReactionIndexNucleosome = j;
                    return;
                }
            }
    }
}


void updatePossibleReactions(chromatin& state, Simparams& sp, Simvars& sv)
{
    // if the enzyme is depleted, we need to update at least the whole channel ...
    // that can be done faster though, but laziness ...
    // if depleted enzyme is available again, we also need to update whole channel
    if (sv.enzConcentr[ sp.c2e[sv.iReactionIndexChannel] ] < 2 && ! sp.nonDynamiConcentrations)
        updateReactionsInRange(state, sp, sv, 0, sp.nNucleosomes);
    else {
        int min = sv.iReactionIndexNucleosome  - sp.buffer +  sp.maxRuleReach;
        if (min < 0) // there is a buffer, yes, but we dont want to check beyond the actual nucstring ...
            min =0;
        uint max = std::min (sp.nNucleosomes , sv.iReactionIndexNucleosome + sp.buffer - sp.maxRuleReach);

        updateReactionsInRange(state, sp, sv, min,max);
        if (sp.cyclic){
            if (min > sp.maxRuleReach )
                updateReactionsInRange(state, sp, sv, sp.nNucleosomes - sp.maxRuleReach + min, sp.nNucleosomes);
            else if (max + sp.maxRuleReach > sp.nNucleosomes )
                updateReactionsInRange(state, sp, sv,0, max + sp.maxRuleReach -sp.nNucleosomes);
        }
    }
}
void initPossibleReactions(  chromatin& chromatin, Simparams& sp, Simvars& sv)
{
    updateReactionsInRange(chromatin, sp, sv, 0, sp.nNucleosomes);
}
void updateReactionsInRange(chromatin& chromatin, Simparams& sp, Simvars& sv,uint min, uint max)
{
    uint mismatch; // pseudo boolean, "true" for anything > 0
    uint partrule; // placeholder for one nucleosome of a rule
    uint rstatus;  // placeholder for one nucleosome of a potential matching region
    uint size = (2 * sp.maxRuleReach + 1) * params::sitevector.size();
    for(uint inuc = min ; inuc <= max; inuc++)
    {
        for(uint c = 0; c < sp.iNumChannelGroups; c+=2)
        {
            if(sv.boundEnzymes[inuc + sp.buffer] >= 0 || (sv.enzConcentr[ sp.c2e[c] ] < 1 && ! sp.nonDynamiConcentrations) )
            {
                if (sv.bmReactionRates[c][inuc+ sp.buffer])
                    sv.channelSiteCount[c]--;
                sv.bmReactionRates[c][inuc+ sp.buffer] = 0;
                continue;
            }
            mismatch =0;
            // iterate over nucleosomes whithin the range of the largest enzyme
            for (uint p = 0 ; p <  size ; ++p) {
                partrule = sp.enzymes[sp.c2e[c]].rules[sp.c2r[c]].target.intString[p];
                rstatus = chromatin.nucleosomestring[(inuc + 2 * sp.maxRuleReach )*params::sitevector.size() + p];
                // there are two ways to get mismatch=0: 1) rule = pattern 2) rule = 0 = a wildcard
                mismatch += (partrule != rstatus) * partrule ;
            }
            // update the channel site count and the reaction matrix
            // next line is best understood by going through all 4 possibilities
            sv.channelSiteCount[c] += (mismatch == 0) - sv.bmReactionRates[c][inuc+ sp.buffer];
            sv.bmReactionRates[c][inuc+ sp.buffer] = (mismatch == 0) ;
        }
    }
}
void updatePropensitySum( Simparams& sp, Simvars& sv)
{
    // dissociation reactions are context independent ... :
    sv.propSum = sv.prePropSum;
    // association reactions:
    for(uint i = 0; i < 2 * sp.numRules ; i+=2)
        sv.propSum += sv.channelSiteCount[i] * sv.channelRates[i];
}

void applyRule(chromatin& chromatin, const Rule& rule, Simvars& sv, Simparams& sp)
{
    for ( uint i = 0; i < params::sitevector.size(); i++)
        if (rule.intWrite[i]>0)
            chromatin.nucleosomestring[(sv.iReactionIndexNucleosome + sp.buffer)*params::sitevector.size() + i ] = rule.intWrite[i];

    if (sp.cyclic){
        uint bufferIndex = 0;
        //modify back bufer if front is changed
        if (sv.iReactionIndexNucleosome < sp.maxRuleReach){
            for ( uint i = 0; i < params::sitevector.size(); i++)
                if (rule.intWrite[i]>0)
                    chromatin.nucleosomestring[(sv.iReactionIndexNucleosome +sp.nNucleosomes + sp.buffer)*params::sitevector.size() + i ] = rule.intWrite[i];
        }
        // modify front buffer if back is changed
        if (sv.iReactionIndexNucleosome + sp.maxRuleReach > sp.nNucleosomes ){

            for ( uint i = 0; i < params::sitevector.size(); i++)
                if (rule.intWrite[i]>0)
                    chromatin.nucleosomestring[(sv.iReactionIndexNucleosome - sp.nNucleosomes + sp.buffer)*params::sitevector.size() + i ] = rule.intWrite[i];

        }
    }
}

void printVectorToFile(std::vector<std::string> vector, std::ofstream& Handle)
{
    unsigned long size = vector.size();
    for(uint i = 0; i < size; i++)
        Handle << "Element: " << i << " Value: " << vector[i] << "\n";
}

void printVectorToFile(std::vector<int> vector, std::ofstream& Handle)
{
    unsigned long size = vector.size();
    for(uint i = 0; i < size; i++)
    {
        //std::cout << "Element: " << i << " Value: " << vector[i] << "\n";
        Handle << vector[i] << "\t";
    }
    Handle << "\n";
}

void printVectorToFile(std::vector<float> vector, std::ofstream& Handle)
{
    unsigned long size = vector.size();
    for(uint i = 0; i < size; i++)
    {
        //std::cout << "Element: " << i << " Value: " << vector[i] << "\n";
        Handle << vector[i] << "\t";
    }
    Handle << "\n";
}

void printMatrixToFile(std::vector<std::vector< float> >& matrix, std::ofstream& Handle)
{
    unsigned long numCols = matrix[0].size();
    unsigned long numRows = matrix.size();

    for(uint i = 0; i < numRows; i++)
    {
        for(uint j = 0; j < numCols; j++)
        {
            Handle << matrix[i][j] << "\t";
        }

        Handle << "\n";
    }
}

std::string writeCSV(std::vector<std::vector<float> > matrix, std::vector<std::string> rowDesc, std::vector<std::string> colDesc)
{
    if(!rowDesc.empty())
    {}


    unsigned long  numCols = matrix[0].size();
    unsigned long numRows = matrix.size();
    std::string sRetStr = "";

    if(!colDesc.empty())
    {
        sRetStr += ";";
        for(uint i = 0; i < numCols; i++)
        {
            sRetStr += colDesc[i] +";";
        }
        sRetStr += "\n";
    }

    for(uint i = 0; i < numRows; i++)
    {
        if(!rowDesc.empty())
        {
            sRetStr += rowDesc[i] + ";";
        }
        for(uint j = 0; j < numCols; j++)
        {
            sRetStr += std::to_string(matrix[i][j]) + ";";
        }
        sRetStr += "\n";
    }


    return sRetStr;
}
