/*
 * histone.hpp
 *
 *  Created on: Jul 11, 2016
 *      Author: nicoh
 */

#ifndef HISTONE_HPP_
#define HISTONE_HPP_

#include <string>
#include <vector>
#include "params.hpp"

struct Hist
{
    public:
    uint imod;   // convert to string using params::modvector [ imod ]
    uint isite;   // convert with sitevector
};

uint mod_to_imod(std::string& mod, std::vector<std::string> &translationTable);

bool is_unmodified(Hist &hist);
uint getHistValue(std::vector<Hist> &nucleosome, uint isite);



#endif /* HISTONE_HPP_ */
